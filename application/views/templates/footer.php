<footer id="footer" class="bg-one">
    <div class="top-footer">
        <div class="container">
            <div class="row justify-content-around">
                <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
                    <ul>
                        <li>
                            <h3>Halaman</h3>
                        </li>
                        <li><a href="<?= base_url() ?>page/view/berita">Berita</a></li>
                        <li><a href="<?= base_url() ?>page/view/pendaftaran">Pendaftaran</a></li>
                        <li><a href="<?= base_url() ?>page/view/kontak">Contact Us</a></li>
                    </ul>
                </div>
                <!-- End of .col-sm-3 -->

                <div class="col-lg-3 col-md-6">
                    <h3>Media Sosial</h3>

                    <div class="social-icon">
                        <ul>
                            <li>
                                <a href="<?= $this->session->userdata('username_fb') ?>"><i class="tf-ion-social-facebook"></i></a>
                            </li>
                            <li>
                                <a href="<?= $this->session->userdata('username_tw') ?>"><i class="tf-ion-social-twitter"></i></a>
                            </li>
                            <li>
                                <a href="<?= $this->session->userdata('username_ig') ?>"><i class="tf-ion-social-instagram"></i></a>
                            </li>
                            <li>
                                <a href="<?= $this->session->userdata('username_lk') ?>"><i class="tf-ion-social-linkedin-outline"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- End of .col-sm-3 -->
            </div>
        </div>
        <!-- end container -->
    </div>
    <div class="footer-bottom">
        <h5>&copy; Copyright <?php echo date("Y") ?>. All rights reserved.</h5>
        <h6>
            Design and Developed by
            <a href="#">Imaji</a>
        </h6>
    </div>
</footer>
<!-- end footer -->

<!-- Essential Scripts =====================================-->
<!-- Main jQuery -->
<script src="<?= base_url() ?>assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap4 -->
<script src="<?= base_url() ?>assets/plugins/bootstrap/bootstrap.min.js"></script>
<!-- Parallax -->
<script src="<?= base_url() ?>assets/plugins/parallax/jquery.parallax-1.1.3.js"></script>
<!-- lightbox -->
<script src="<?= base_url() ?>assets/plugins/lightbox2/js/lightbox.min.js"></script>
<!-- Owl Carousel -->
<script src="<?= base_url() ?>assets/plugins/slick/slick.min.js"></script>
<!-- filter -->
<script src="<?= base_url() ?>assets/plugins/filterizr/jquery.filterizr.min.js"></script>
<!-- Smooth Scroll js -->
<script src="<?= base_url() ?>assets/plugins/smooth-scroll/smooth-scroll.min.js"></script>

<!-- Custom js -->
<script src="<?= base_url() ?>assets/js/script.js"></script>
<!-- treeMaker Js -->
<script src="<?= base_url() ?>assets/plugins/treeMaker/tree_maker-min.js"></script>
<!-- daterangepicker Js -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<!-- Highcharts -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/sankey.js"></script>
<script src="https://code.highcharts.com/modules/organization.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<!-- Custom Page Js -->
<script src="<?= base_url() ?>assets/js/page/<?= $page ?>.js"></script>
</body>

</html>
