<div class="hero-slider" style="margin-top: 82px">
    <?php foreach ($slider as $s) : ?>
        <div class="slider-item th-fullpage hero-area" style="background-image: url(<?= base_url() ?>assets/images/slider/<?= $s['nama_gambar'] ?>)">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1 data-duration-in=".3" data-animation-in="fadeInUp" data-delay-in=".1">
                            <?= $s['judul'] ?>
                        </h1>
                        <p data-duration-in=".3" data-animation-in="fadeInUp" data-delay-in=".5">
                            <?= $s['sub_judul'] ?>
                        </p>
                        <a data-duration-in=".3" data-animation-in="fadeInUp" data-delay-in=".8" class="btn btn-main" href="<?= $s['link'] ?>">Explore Us</a>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach ?>
</div>
