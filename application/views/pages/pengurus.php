<section class="services" id="services">
    <div class="container">
        <?php foreach ($desc_pengurus as $p) : ?>
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="text-center mb-5">
                        <h2><?= $p['judul']; ?></h2>
                        <p><?= $p['deskripsi']; ?></p>
                        <div class="border"></div>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
        <!-- <figure class="highcharts-figure"> -->
        <div id="container"></div>
        <!-- </figure> -->
    </div>
</section>