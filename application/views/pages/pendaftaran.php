<section class="about" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10 contact-form">
                <h3 class="mb-5">Pendaftaran Anggota Baru Inisiatif Masyarakat Jurnal Indonesia (IMAJI)</h3>
                <?php echo @$this->session->flashdata('msg'); ?>
                <div class="alert alert-warning f-bold" role="alert">
                    <i class="tf-ion-android-warning"></i>
                    Siapkan pas foto untuk kartu anggota, foto kartu identitas (KTP/SIM) dan KTM bagi Mahasiswa dalam bentuk jpg/jpeg (maks 1MB)
                </div>
                <?php echo form_open_multipart('pendaftaran/daftar', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="form-group row mt-5">
                    <label class="col-md-4 mt-2 c-form f-bold">Nama Lengkap : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="nama_lengkap" placeholder="Masukan nama lengkap" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Gelar Depan :</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="gelar_depan" placeholder="Misalnya Ir. Prof. H">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Gelar Belakang :</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="gelar_belakang" placeholder="Misalnya M.T.I, M.Kom">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Pas Poto : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="file" class="form-control mb-0" name="pas_foto" required>
                        <span class="badge badge-info mt-0">Maximal 1MB</span>
                    </div>
                </div>
                <div class="form-group row">
                    <!-- <input type="date"> -->
                    <label class="col-md-4 mt-2 c-form f-bold">Tempat Lahir : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="tempat_lahir" placeholder="Masukan tempat lahir" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Tanggal Lahir : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="tanggal_lahir" name="tanggal_lahir" placeholder="Masukan tanggal lahir" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">NIK : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="no_nik" placeholder="Masukan NIK" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Foto Kartu Identitas (SIM/KTP) : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="file" class="form-control" name="kartu_identitas" required>
                        <span class="badge badge-info mt-0">Maximal 1MB</span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Alamat : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <textarea class="form-control" cols="10" rows="2" name="alamat" placeholder="Masukan Alamat" required></textarea>
                        <span class="badge badge-info mt-0">Alamat ini akan digunakan untuk pengiriman kartu anggota</span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold" for="provinsi">Provinsi : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <select class="form-control" id="provinsi" name="provinsi" required>
                            <option disabled value selected>Pilih Provinsi</option>
                            <?php
                            foreach ($provinsi as $row) {
                                echo '<option value="' . $row->name . '">' . $row->name . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold" for="kota">Kota/Kabuppaten : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <select class="form-control" id="kota" name="kota" required>
                            <?php ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Kode POS : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="kode_pos" placeholder="Masukan kode pos" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">No HP : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="no_hp" placeholder="Masukan no HP" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">No Telepon Kantor :</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="no_telp_kantor" placeholder="Masukan No telp kantor">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Email Pribadi : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="email" class="form-control" name="email_pribadi" placeholder="Masukan email pribadi" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Email Bisnis :</label>
                    <div class="col-md-8">
                        <input type="email" class="form-control" name="email_bisnis" placeholder="Masukan email bisnis">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Nama Institusi : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="nama_institusi" placeholder="Masukan nama institusi" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Alamat Institusi : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <textarea class="form-control" cols="10" rows="2" name="alamat_institusi" placeholder="Masukan alamat institusi" required></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold" for="provinsi">Provinsi Institusi : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <select class="form-control" id="provinsi_institusi" name="provinsi_institusi" required>
                            <option disabled value selected>Pilih Provinsi</option>
                            <?php
                            foreach ($provinsi as $row) {
                                echo '<option value="' . $row->name . '">' . $row->name . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold" for="kota-kabupaten">Kota/Kabuppaten Institusi : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <select class="form-control" id="kota_institusi" name="kota_institusi" required>
                            <?php ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Kode POS Institusi : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="kode_pos_institusi" placeholder="Masukan kode pos institusi" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Program Studi S1 : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="program_studi" placeholder="Masukan program studi" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Bidang keahlian : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="bidang_keahlian" placeholder="Contoh : Programming" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Scopus ID : </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="scopus_id" placeholder="Scopus ID">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Orcid ID : </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="orcid_id" placeholder="Orcid ID">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Google Scholar ID : </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="google_s_id" placeholder="Google Scholar ID">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Sinta ID : </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="sinta_id" placeholder="Sinta ID">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Publon ID : </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="publon_id" placeholder="Publon ID">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 mt-2 c-form f-bold">Profesi : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <select class="form-control" name="profesi" id="profesi" required>
                            <option disabled selected value>Pilih Profesi </option>
                            <option value="pengelola_jurnal">Pengelola Jurnal</option>
                            <option value="mahasiswa">Mahasiswa</option>
                            <option value="dosen">Dosen</option>
                            <option value="umum">Umum</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row" id="nama_jurnal">
                    <label class="col-md-4 mt-2 c-form f-bold">Nama Jurnal : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="nama_jurnal" id="nama_jurnala" placeholder="Nama Jurnal" required>
                    </div>
                </div>
                <div class="form-group row" id="pic">
                    <label class="col-md-4 mt-2 c-form f-bold">PIC : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="pic" id="pica" placeholder="PIC" required>
                    </div>
                </div>
                <div class="form-group row" id="issn">
                    <label class="col-md-4 mt-2 c-form f-bold">ISSN : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="issn" id="issna" placeholder="ISSN" required>
                    </div>
                </div>
                <div class="form-group row" id="url_jurnal">
                    <label class="col-md-4 mt-2 c-form f-bold">URL Jurnal : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="url_jurnal" id="url_jurnala" placeholder="URL Jurnal" required>
                    </div>
                </div>
                <div class="form-group row" id="nim">
                    <label class="col-md-4 mt-2 c-form f-bold">NIM : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="nim" id="nima" placeholder="Nim" required>
                    </div>
                </div>
                <div class="form-group row" id="foto_ktm">
                    <label class="col-md-4 mt-2 c-form f-bold">Foto KTM : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="file" class="form-control" name="foto_ktm" id="foto_ktma" required>
                        <span class="badge badge-info mt-0">Maximal 1MB</span>
                    </div>
                </div>
                <div class="form-group row" id="administrasi_profesi">
                    <label class="col-md-4 mt-2 c-form f-bold">Administrasi Profesi : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <select class="form-control" name="administrasi_profesi" id="administrasi_profesi_value" required>
                            <option disabled selected value>Pilih Administrasi </option>
                            <option value="150000">1 Tahun Rp 150.000</option>
                            <option value="250000">2 Tahun Rp 250.000</option>
                            <option value="350000">3 Tahun Rp 350.000</option>
                        </select>
                        <select class="form-control" name="administrasi_profesi" id="administrasi_mahasiswa_value">
                            <option disabled selected value>Pilih Administrasi </option>
                            <option value="50000">1 Tahun Rp 50.000</option>
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn btn-pendaftaran mt-20 shadow-sm">Daftar Sekarang</button>
                </form>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</section>