<section class="about" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-9 contact-form">
                <h3 class="mb-5">Perpanjangan Anggota</h3>
                <?php echo @$this->session->flashdata('msg'); ?>
                <?php echo form_open_multipart('perpanjangan', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="form-group row mt-5">
                    <label class="col-md-4 mt-2 c-form f-bold">Kode Anggota : <span class="text-red">(*)</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="kode_anggota" placeholder="Masukan kode anggota" required>
                    </div>
                </div>
                <button type="submit" class="btn btn-pendaftaran mt-20 shadow-sm">Submit</button>
                </form>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</section>