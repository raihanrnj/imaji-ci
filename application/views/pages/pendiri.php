<section class="services" id="services">
    <div class="container">
        <?php foreach ($desc_pendiri as $d) : ?>
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="text-center mb-5">
                        <h2><?= $d['judul']; ?></h2>
                        <p><?= $d['deskripsi']; ?></p>
                        <div class="border"></div>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
        <div class="row justify-content-center">
            <?php foreach ($pendiri as $p) : ?>
                <div class="col-md-3 col-sm-6 mb-4">
                    <div class="card">
                        <img src="<?= base_url() ?>assets/images/pendiri/<?= $p['foto']; ?>" alt="foto pendiri" class="img-fluid ml-3 mr-3 mt-3 mb-3" style="min-height: 200px; max-height: 200px;">
                        <div class="card-body">
                            <h5 class="text-center"><?= $p['nama']; ?></h5>
                            <p class="text-center" style="font-size: 15px;"><?= $p['afiliasi']; ?></p>
                            <p class="text-center" style="font-size: 15px;"><?= $p['alamat']; ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
    </div>
</section>