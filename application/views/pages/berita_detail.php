<!-- blog details part start -->
<section class="blog-details section">
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-8">
                <?php
                $b = $berita->row_array();
                ?>
                <article class="post">
                    <div class="post-image mb-5">
                        <img loading="lazy" class="img-fluid w-100" src="<?= base_url() ?>assets/images/berita/<?php echo $b['berita_image']; ?>" alt="post-image" />
                    </div>
                    <!-- Post Content -->
                    <div class="post-content">
                        <h3><?php echo $b['berita_judul']; ?></h3>
                        <ul class="list-inline">
                            <li class="list-inline-item"><?php $tanggal = $b['berita_tanggal'];
                                                            echo date('d F Y', strtotime($tanggal)); ?></li>
                        </ul>
                        <?php echo $b['berita_isi']; ?>
                    </div>
                </article>
            </div>
            <div class="col-lg-4 mt-5 mt-lg-0">
                <!-- sidebar -->
                <aside class="sidebar pl-0 pl-lg-4">
                    <div class="widget-post widget">
                        <h2 style="color: black;">Latest Post</h2>
                        <!-- latest post -->
                        <ul class="widget-post-list">
                            <?php foreach ($berita_terbaru as $key) : ?>
                                <li class="widget-post-list-item">
                                    <!-- <div class="widget-post-image"> -->
                                    <a href="<?= base_url() ?>page/views/<?= $key['berita_id'] ?>">
                                        <img class="mr-3" loading="lazy" src="<?= base_url() ?>assets/images/berita/<?= $key['berita_image'] ?>" alt="post-img" style="width: 120px; height: 80px;" />
                                    </a>
                                    <!-- </div> -->
                                    <div class="widget-post-content">
                                        <a href="<?= base_url() ?>page/views/<?= $key['berita_id'] ?>">
                                            <h5><?= $key['berita_judul'] ?></h5>
                                        </a>
                                        <p style="color: #aaa;"><?php $tanggal = $key['berita_tanggal'];
                                                                echo date('d F Y', strtotime($tanggal)) ?></p>
                                    </div>
                                </li>
                            <?php endforeach ?>

                        </ul>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</section>
<!-- blog details part end -->