<div class="container bg-gray">
    <div class="btn-group float-right mt-5" role="group" aria-label="Basic example">
        <a href="<?= base_url() ?>page/view/berita" class="btn btn-outline-info"> Terbaru</a>
        <a href="<?= base_url() ?>page/view/berita_old" class="btn btn-info"><i class="fa fa-filter" aria-hidden="true"></i> Terlama</a>
    </div>
</div>
<section class="posts section bg-gray mt-4">
    <div class="container">
        <div class="row">
            <!-- single blog post -->
            <?php
            function limit_words($string, $word_limit)
            {
                $words = explode(" ", $string);
                return implode(" ", array_splice($words, 0, $word_limit));
            }
            foreach ($berita as $b) : ?>
                <article class="col-lg-4 col-md-6">
                    <div class="post-item">
                        <div class="media-wrapper">
                            <img loading="lazy" src="<?= base_url() ?>assets/images/berita/<?php echo $b->berita_image ?>" alt="Cover Berita" style="min-height: 196px;" class="img-fluid" />
                        </div>
                        <div class="content">
                            <h3><a href="<?= base_url() ?>page/views/<?= $b->berita_id ?>"><?php echo $b->berita_judul ?></a></h3>
                            <?php echo limit_words($b->berita_isi, 30) ?> ...<br>
                            <a class="btn btn-main" href="<?= base_url() ?>page/views/<?= $b->berita_id ?>">Read more</a>
                        </div>
                    </div>
                </article>
            <?php endforeach ?>
            <!-- /single blog post -->
        </div>
    </div>
    <div class="my-5">
        <?php echo $this->pagination->create_links(); ?>
    </div>
</section>