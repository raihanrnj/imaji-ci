<!-- Start About Section ==================================== -->
<?php foreach ($intro as $i) : ?>
    <section class="about-2 section" id="about" style="margin-top: -82px">
        <div class="container">
            <div class="row justify-content-center">
                <!-- section title -->
                <div class="col-lg-12">
                    <div class="title text-center">
                        <h2><?= $i['judul'] ?></h2>
                        <div class="border"></div>
                    </div>
                </div>
                <!-- /section title -->
            </div>

            <div class="row">
                <div class="col-md-6 mb-4 mb-md-0">
                    <img loading="lazy" src="<?= base_url() ?>assets/images/intro/<?= $i['gambar'] ?>" class="img-fluid" alt="" />
                </div>
                <div class="col-md-6">
                    <p style="line-height: 34px">
                        <?= $i['isi'] ?>
                    </p>
                    <a href="<?= base_url() ?>page/view/profiles" class="btn btn-main mt-20">Read More</a>
                </div>
            </div>
            <!-- End row -->
        </div>
        <!-- End container -->
    </section>
<?php endforeach ?>
<!-- End section -->
<!-- Start Blog Section =========================================== -->
<section class="blog" id="blog">
    <div class="container">
        <div class="row justify-content-center">
            <!-- section title -->
            <div class="col-xl-6 col-lg-8">
                <div class="title text-center">
                    <h2>News</h2>
                    <div class="border"></div>
                </div>
            </div>
            <!-- /section title -->
        </div>

        <div class="row">
            <!-- single blog post -->
            <?php
            function limit_words($string, $word_limit)
            {
                $words = explode(" ", $string);
                return implode(" ", array_splice($words, 0, $word_limit));
            }
            foreach ($berita as $b) : ?>
                <article class="col-lg-4 col-md-6">
                    <div class="post-item">
                        <div class="media-wrapper">
                            <img loading="lazy" src="<?= base_url() ?>assets/images/berita/<?= $b['berita_image'] ?>" alt="Cover Berita" style="min-height: 196px;" class="img-fluid" />
                        </div>
                        <div class="content">
                            <h3><a href="<?= base_url() ?>page/views/<?= $b['berita_id'] ?>"><?= $b['berita_judul'] ?></a></h3>
                            <?= limit_words($b['berita_isi'], 30) ?> ...<br>
                            <a class="btn btn-main" href="<?= base_url() ?>page/views/<?= $b['berita_id'] ?>">Read more</a>
                        </div>
                    </div>
                </article>
            <?php endforeach ?>
            <!-- /single blog post -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</section>
<!-- end section -->

<!-- Start Portfolio Section =========================================== -->
<section class="portfolio section-sm" id="portfolio">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8">
                <!-- section title -->
                <div class="title text-center">
                    <h2>Gallery</h2>
                    <div class="border"></div>
                </div>
                <!-- /section title -->
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-12">
                        <div class="filtr-container">
                            <?php foreach ($galeri as $g) : ?>
                                <div class="col-md-3 col-sm-6 col-xs-6">
                                    <div class="portfolio-block">
                                        <img class="img-fluid" src="<?= base_url() ?>assets/images/galeri/<?= $g['nama_gambar'] ?>" alt="" />
                                        <div class="caption">
                                            <a class="search-icon" href="<?= base_url() ?>assets/images/galeri/<?= $g['nama_gambar'] ?>" data-lightbox="image-1">
                                                <i class="tf-ion-ios-search-strong"></i>
                                            </a>
                                            <h4><a href=""><?= $g['judul'] ?></a></h4>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /end col-lg-12 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
    <!-- <center>
        <a href="" class="btn btn-main text-center mt-20">Load More</a>
    </center> -->
</section>
<!-- End section -->