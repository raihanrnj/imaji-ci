<section class="about-shot-info section-sm">
    <div class="container">
        <?php foreach ($keanggotaan as $key) : ?>
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="title text-center">
                        <h3>Syarat Keanggotaan</h3>
                        <div class="border"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 mb-5 mb-lg-0">
                    <img loading="lazy" src="<?= base_url() ?>assets/images/<?= $key['gambar'] ?>" class="img-fluid" alt="" />
                </div>
                <div class="col-lg-6">
                    <ul class="checklist" style="padding-left: 0px !important;">
                        <?= $key['isi'] ?>
                    </ul>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</section>

<section class="about-shot-info section-sm" id="about">
    <div class="container">
        <?php foreach ($info_keanggotaan as $keys) : ?>

            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="title text-center">
                        <h3>Daftar Menjadi Anggota</h3>
                        <div class="border"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <ul class="checklist" style="padding-left: 0px !important;">
                        <?= $keys['isi'] ?>
                    </ul>
                    <a href="<?= base_url() ?>page/view/pendaftaran" class="btn btn-main mt-20">Daftar Sekarang</a><br />
                    <a href="<?= base_url() ?>page/view/perpanjangan" class="btn btn-outline-main mt-20 pl-5 pr-5">&nbsp;Perpanjangan</a>
                </div>
                <div class="col-lg-6 mt-5 mb-lg-0">
                    <img loading="lazy" src="<?= base_url() ?>assets/images/keanggotaan/<?= $keys['gambar'] ?>" class="img-fluid" alt="" />
                </div>
            </div>
        <?php endforeach ?>
    </div>
</section>