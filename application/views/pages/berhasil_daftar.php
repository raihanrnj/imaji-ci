<section class="about mt-5">
    <div class="container">
        <div class="alert alert-success" style="line-height: 150%;" role="alert">
            <i class="tf-ion-android-success"></i>
            Selamat data anda sukses terekam. <br />
            Untuk mengaktifkan akun anda, silahkan transfer uang keanggotan ke rekening berikut : <br />
            <div class="my-3">
                <div class="f-bold">No rek : 1175326719 (BNI)</div>
                <div class="f-bold"> a/n : Dwi Astharini</div>
            </div>
            <div class="row ml-0">
                Nominal :&nbsp; Rp.<div class="f-bold"><?php echo @$this->session->flashdata('msg'); ?></div>             
            </div>
            Tambahkan catatan transfer : Nominal + 3 angka terakhir nomer HP anda <br />
            Contoh misal nominal 150.000 dan Nomer Hp : 083123456789 <br />
            Maka catatan transfer = <b> 150.789 </b>  <br /><br />
            Kemudian kirim bukti transfer ke email <b>member@masyarakatjurnal.or.id</b> dengan SUBJECT <br />
            <div class="f-bold">“KONFIRMASI PEMBAYARAN IMAJI - [NIK KTP anda]”</div> <br/>
        </div>
    </div>
</section>