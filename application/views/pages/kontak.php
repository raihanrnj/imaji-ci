 <!--Start Contact Us =========================================== -->
 <section class="contact-us" id="contact">
     <div class="container">
         <div class="row">
             <!-- Contact Details -->
             <?php foreach ($contact as $a) : ?>
                 <div class="contact-details col-md-12 ">
                     <h3 class="mb-3">Kontak Detail</h3>
                     <p>Inisiatif Masyarakat Jurnal Indonesia (IMAJI)</p>
                     <ul class="checklistt mt-4">
                         <li class="mb-3">
                             <i class="fa fa-building" aria-hidden="true"></i>
                             <span> <?= $a['alamat'] ?></span>
                         </li>
                         <li class="mb-3">
                             <i class="tf-ion-social-whatsapp" aria-hidden="true"></i>
                             <span><?= $a['no_hp'] ?></span>
                         </li>
                         <li>
                             <i class="tf-ion-android-mail"></i>
                             <span><?= $a['email'] ?></span>
                         </li>
                     </ul>
                     <!-- Footer Social Links -->
                     <div class="social-icon">
                         <ul>
                             <li><a href="<?= $a['username_fb'] ?>"><i class="tf-ion-social-facebook"></i></a></li>
                             <li><a href="<?= $a['username_tw'] ?>"><i class="tf-ion-social-twitter"></i></a></li>
                             <li><a href="<?= $a['username_ig'] ?>"><i class="tf-ion-social-instagram"></i></a></li>
                             <li><a href="<?= $a['username_lk'] ?>"><i class="tf-ion-social-linkedin"></i></a></li>
                         </ul>
                     </div>
                     <!--/. End Footer Social Links -->
                 </div>
             <?php endforeach ?>
             <!-- / End Contact Details -->
         </div>
     </div> <!-- end container -->
 </section>
 <!-- end Contact section -->