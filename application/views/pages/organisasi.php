<section class="services" id="services">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8">
                <div class="text-center mb-5">
                    <h2>Dewan Pendiri</h2>
                    <p>Dewan Pendiri Ikatan Ahli Informatika Indonesia (IAAI)</p>
                    <div class="border"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <img src="<?= base_url() ?>assets/images/about/about-header.jpg" alt="" srcset="" class="img-fluid">
                    <div class="card-body">
                        <h5 class="text-center my-3">Heri Irawan</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <img src="<?= base_url() ?>assets/images/about/about-header.jpg" alt="" srcset="" class="img-fluid ml-3 mr-3 mt-3 mb-3">
                    <div class="card-body">
                        <h5 class="text-center my-3">Heri Irawan</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

<section class="team-skills section-sm" id="skills">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8">
                <div class="text-center mb-5">
                    <h2>Pengurus</h2>
                    <p>Pengurus Ikatan Ahli Informatika Indonesia (IAAI)</p>
                    <div class="border"></div>
                </div>
            </div>
            <div id="chart_pengurus"></div>
        </div>
</section>