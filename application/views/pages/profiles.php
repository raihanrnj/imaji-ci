<?php foreach ($pengantar as $p) : ?>
    <section class="about-shot-info section-sm">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mt-20">
                    <h2 class="mb-3"><?= $p['judul'] ?></h2>
                    <?= $p['isi'] ?>
                </div>
            </div>
        </div>
    </section>
<?php endforeach ?>

<?php foreach ($akta_pendirian as $a) : ?>
    <section class="about-shot-info section-sm bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mt-20">
                    <h2 class="mb-3"><?= $a['judul'] ?></h2>
                    <?= $a['isi'] ?>
                </div>
                <div class="col-lg-6 mt-4 mt-lg-0">
                    <img loading="lazy" class="img-fluid" src="<?= base_url() ?>assets/images/company/<?= $a['gambar'] ?>" alt="" />
                </div>
            </div>
        </div>
    </section>
<?php endforeach ?>

<?php foreach ($lambang_dan_arti as $l) : ?>
    <section class="about-2 section" id="about">
        <div class="container">
            <div class="row justify-content-center">
                <!-- section title -->
                <div class="col-lg-6">
                    <div class="title text-center">
                        <h2><?= $l['judul'] ?></h2>
                    </div>
                </div>
                <!-- /section title -->
            </div>

            <div class="row">
                <div class="col-md-6 mb-4 mb-md-0">
                    <img loading="lazy" src="<?= base_url() ?>assets/images/lambang-dan-arti/<?= $l['gambar'] ?>" class="img-fluid" alt="" />
                </div>
                <div class="col-md-6">
                    <?= $l['isi'] ?>
                </div>
            </div>
            <!-- End row -->
        </div>
        <!-- End container -->
    </section>
<?php endforeach ?>

<?php foreach ($visi_misi as $v) : ?>
    <section class="company-mission section-sm bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3>Visi</h3>
                    <?= $v['visi'] ?>
                </div>
                <div class="col-md-6 mt-5 mt-md-0">
                    <h3>Misi</h3>
                    <ul class="checklist" style="margin-left: 10px">
                        <?php echo "<li>", $v['misi'], "</li>" ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<?php endforeach ?>

<?php foreach ($kode_etik as $k) : ?>
    <section class="about-shot-info section-sm">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mt-20">
                    <h2 class="mb-3">Kode Etik</h2>
                    <ul class="checklist" style="padding-left: 0px !important;">
                        <?= $k['isi'] ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<?php endforeach ?>