<div class="msg" style="display:none;">
    <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="box">
    <div class="box-header">
        <div class="col-md-4">
            <button class="form-control btn btn-primary" data-toggle="modal" data-target="#modal-default"><i class="glyphicon glyphicon-plus-sign"></i> Tambah Data</button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="list-data" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Alamat</th>
                    <th>HP</th>
                    <th>Email</th>
                    <th style="text-align: center;">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = $this->uri->segment('3') + 1;
                foreach ($contact as $s) : ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $s['alamat'] ?></td>
                        <td><?= $s['no_hp'] ?></td>
                        <td><?= $s['email'] ?></td>
                        <td>
                            <a class="btn btn-info" style="margin-top: 4px;" href="" data-toggle="modal" data-target="#detail_data<?= $s['id'] ?>"><i class="glyphicon glyphicon-eye-open"></i> </a>
                            <a class="btn btn-warning" style="margin-top: 4px;" href="" data-toggle="modal" data-target="#modal-update_data<?= $s['id'] ?>"><i class="glyphicon glyphicon-edit"></i> </a>
                            <a class="btn btn-danger" style="margin-top: 4px;" href="" data-toggle="modal" data-target="#modal-delete_data<?= $s['id'] ?>"><i class="glyphicon glyphicon-trash"></i> </a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>


<!-- Modal Add data -->
<div class="modal fade" id="modal-default">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Kontak</h4>
            </div>
            <?php echo form_open_multipart('user_contact/contact_add', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group">
                        <label for="alamat" class="col-sm-2 control-label">Alamat</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="alamat" cols="30" rows="2" placeholder="Masukan Alamat" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="no_hp" class="col-sm-2 control-label">Telphone</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="no_hp" placeholder="Ex. 085234543234" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" name="email" placeholder="Ex. hello@gmail.com" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="username_fb" class="col-sm-2 control-label">Facebook</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="username_fb" placeholder="https://web.facebook.com/username" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="username_tw" class="col-sm-2 control-label">Twitter</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="username_tw" placeholder="Ex. https://twitter.com/username" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="username_ig" class="col-sm-2 control-label">Instagram</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="username_ig" placeholder="https://www.instagram.com/username" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="username_lk" class="col-sm-2 control-label">Linkedin</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="username_lk" placeholder="https://www.linkedin.com/in/username" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Modal Update data -->
<?php
$no = $this->uri->segment('3') + 1;
foreach ($contact as $s) : ?>
    <div class="modal fade" id="modal-update_data<?= $s['id'] ?>">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Contact</h4>
                </div>
                <?php echo form_open_multipart('user_contact/contact_edit', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="alamat" class="col-sm-2 control-label">Alamat</label>
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" name="id" placeholder="Ex. 085234543234" value="<?= $s['id'] ?>">
                                <textarea class="form-control" name="alamat" cols="30" rows="2" placeholder="Masukan Alamat" required><?= $s['alamat'] ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="no_hp" class="col-sm-2 control-label">Telphone</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="no_hp" placeholder="Ex. 085234543234" value="<?= $s['no_hp'] ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" name="email" placeholder="Ex. hello@gmail.com" value="<?= $s['email'] ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="username_fb" class="col-sm-2 control-label">Facebook</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="username_fb" placeholder="https://web.facebook.com/username" value="<?= $s['username_fb'] ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="username_tw" class="col-sm-2 control-label">Twitter</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="username_tw" placeholder="Ex. https://twitter.com/username" value="<?= $s['username_tw'] ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="username_ig" class="col-sm-2 control-label">Instagram</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="username_ig" placeholder="https://www.instagram.com/username" value="<?= $s['username_ig'] ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="username_lk" class="col-sm-2 control-label">Linkedin</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="username_lk" placeholder="https://www.linkedin.com/in/username" value="<?= $s['username_lk'] ?>" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach ?>

<?php
$no = $this->uri->segment('3') + 1;
foreach ($contact as $s) : ?>
    <div class="modal fade" id="detail_data<?= $s['id'] ?>">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detail Contact</h4>
                </div>
                <?php echo form_open_multipart('user_contact/contact_edit', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="alamat" class="col-sm-2 control-label">Alamat</label>
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" name="id" placeholder="Ex. 085234543234" value="<?= $s['id'] ?>">
                                <textarea class="form-control" name="alamat" cols="30" rows="2" placeholder="Masukan Alamat" readonly><?= $s['alamat'] ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="no_hp" class="col-sm-2 control-label">Telphone</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="no_hp" placeholder="Ex. 085234543234" value="<?= $s['no_hp'] ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" name="email" placeholder="Ex. hello@gmail.com" value="<?= $s['email'] ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="username_fb" class="col-sm-2 control-label">Facebook</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="username_fb" placeholder="https://web.facebook.com/username" value="<?= $s['username_fb'] ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="username_tw" class="col-sm-2 control-label">Twitter</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="username_tw" placeholder="Ex. https://twitter.com/username" value="<?= $s['username_tw'] ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="username_ig" class="col-sm-2 control-label">Instagram</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="username_ig" placeholder="https://www.instagram.com/username" value="<?= $s['username_ig'] ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="username_lk" class="col-sm-2 control-label">Linkedin</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="username_lk" placeholder="https://www.linkedin.com/in/username" value="<?= $s['username_lk'] ?>" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach ?>

<!-- Modal hapus data -->
<?php
$no = $this->uri->segment('3') + 1;
foreach ($contact as $s) : ?>
    <div class="modal fade" id="modal-delete_data<?= $s['id'] ?>">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Hapus data ini ?</h4>
                </div>
                <?php echo form_open_multipart('user_contact/contact_delete', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6"><a href="<?= base_url() ?>user_contact/contact_delete/<?= $s['id'] ?>" class="form-control btn btn-primary"> <i class="glyphicon glyphicon-ok-sign"></i> Ya, Hapus Data Ini</a></div>
                            <div class="col-md-6"><button class="form-control btn btn-danger" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Tidak</button></div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach ?>

<div id="tempat-modal"></div>