<div class="msg" style="display:none;">
    <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="box">
    <div class="box-header">
        <div class="col-md-4">
            <button class="form-control btn btn-primary" data-toggle="modal" data-target="#modal-default"><i class="glyphicon glyphicon-plus-sign"></i> Tambah Data</button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="list-data" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Judul</th>
                    <th>Berita</th>
                    <th>Gambar</th>
                    <th>Tanggal Post</th>
                    <th style="text-align: center;">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                function limit_words($string, $word_limit)
                {
                    $words = explode(" ", $string);
                    return implode(" ", array_splice($words, 0, $word_limit));
                }
                $no = $this->uri->segment('3') + 1;
                foreach ($berita as $s) : ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $s['berita_judul'] ?></td>
                        <td><?php echo limit_words($s['berita_isi'], 30) ?> ...</td>
                        <td><img class="img-fluid" style="width: 250px;" src="<?= base_url() ?>assets/images/berita/<?= $s['berita_image']; ?>" alt=""></td>
                        <td><?= $s['berita_tanggal'] ?></td>
                        <td>
                            <a class="btn btn-warning" style="margin-top: 4px;" href="" data-toggle="modal" data-target="#modal-update_data<?= $s['berita_id'] ?>"><i class="glyphicon glyphicon-edit"></i> </a>
                            <a class="btn btn-danger" style="margin-top: 4px;" href="" data-toggle="modal" data-target="#modal-delete_data<?= $s['berita_id'] ?>"><i class="glyphicon glyphicon-trash"></i> </a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>


<!-- Modal Add data -->
<div class="modal fade" id="modal-default">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Berita</h4>
            </div>
            <?php echo form_open_multipart('post_berita/post_berita', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group">
                        <label for="judul" class="col-sm-2 control-label">Judul</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="judul" placeholder="Masukan judul berita" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="judul" class="col-sm-2 control-label">Berita</label>
                        <div class="col-sm-10">
                            <textarea class="textarea" name="berita" placeholder="Masukan konten berita" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama_gambar" class="col-sm-2 control-label">Gambar Thumbnails</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="nama_gambar" placeholder="" required>
                            <small class="label bg-aqua">Maximal 1MB</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Modal Update data -->
<?php
$no = $this->uri->segment('3') + 1;
foreach ($berita as $s) : ?>
    <div class="modal fade" id="modal-update_data<?= $s['berita_id'] ?>">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Image</h4>
                </div>
                <?php echo form_open_multipart('post_berita/berita_edit', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="judul" class="col-sm-2 control-label">Judul</label>
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" name="berita_id" placeholder="" value="<?= $s['berita_id'] ?>" required>
                                <input type="text" class="form-control" name="judul" placeholder="Masukan judul berita" value="<?= $s['berita_judul'] ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="judul" class="col-sm-2 control-label">Berita</label>
                            <div class="col-sm-10">
                                <textarea class="textarea" name="berita" placeholder="Masukan konten berita" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required><?= $s['berita_isi'] ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nama_gambar" class="col-sm-2 control-label">Gambar Thumbnails</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" name="nama_gambar" placeholder="" value="<?= $s['berita_image'] ?>">
                                <small class="label bg-aqua">Maximal 1MB</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach ?>

<!-- Modal hapus data -->
<?php
$no = $this->uri->segment('3') + 1;
foreach ($berita as $s) : ?>
    <div class="modal fade" id="modal-delete_data<?= $s['berita_id'] ?>">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Hapus data ini ?</h4>
                </div>
                <?php echo form_open_multipart('post_berita/berita_delete', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6"><a href="<?= base_url() ?>post_berita/berita_delete/<?= $s['berita_id'] ?>" class="form-control btn btn-primary"> <i class="glyphicon glyphicon-ok-sign"></i> Ya, Hapus Data Ini</a></div>
                            <div class="col-md-6"><button class="form-control btn btn-danger" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Tidak</button></div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach ?>

<div id="tempat-modal"></div>