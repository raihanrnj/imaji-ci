<div class="col-md-offset-1 col-md-10 col-md-offset-1 well">
  <div class="form-msg"></div>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h3 style="display:block; text-align:center;">Update Data Anggota</h3>
      <form method="POST" id="form-update-validasianggota">
      <input type="hidden" name="kode_anggota" value="<?php echo $dataAnggota->kode_anggota; ?>">
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
              Nama
          </span>
          <input type="text" class="form-control" placeholder="Nama" name="nama" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->gelar_depan.' '. $dataAnggota->nama_lengkap.' '.$dataAnggota->gelar_belakang ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Pas Foto
          </span>
          <span class="form-control">
          <a href="assets/images/pendaftaran/<?php echo $dataAnggota->pas_foto; ?>" class="btn btn-xs btn-success"> Tampilkan Gambar </a>
          </span>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Tanggal Join
          </span>
          <input type="text" class="form-control" placeholder="Tanggal Join" name="tanggal_join" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->tanggal_join; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Tanggal Expired
          </span>
          <input type="text" class="form-control" placeholder="Tanggal Expired" name="tanggal_expired" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->tanggal_expired; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Tempat Lahir
          </span>
          <input type="text" class="form-control" placeholder="Tempat Lahir" name="tempat_lahir" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->tempat_lahir; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Tanggal Lahir
          </span>
          <input type="text" class="form-control" placeholder="Tanggal Lahir" name="tanggal_lahir" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->tanggal_lahir; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            NIK
          </span>
          <input type="text" class="form-control" placeholder="NIk" name="no_nik" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->no_nik; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            KTP
          </span>
          <span class="form-control">
          <a href="assets/images/pendaftaran/<?php echo $dataAnggota->kartu_identitas; ?>" class="btn btn-xs btn-success"> Tampilkan Gambar </a>
          </span>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Alamat
          </span>
          <input type="text" class="form-control" placeholder="Alamat" name="alamat" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->alamat; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Provinsi
          </span>
          <input type="text" class="form-control" placeholder="Provinsi" name="provinsi" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->provinsi; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Kota
          </span>
          <input type="text" class="form-control" placeholder="Kota" name="kota" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->kota; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Kode Pos
          </span>
          <input type="text" class="form-control" placeholder="Kode Pos" name="kode_pos" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->kode_pos; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Nomer HP
          </span>
          <input type="text" class="form-control" placeholder="Nomer HP" name="no_hp" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->no_hp; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            No Telp Kantor
          </span>
          <input type="text" class="form-control" placeholder="Nomer Telp Kantor" name="no_telp_kantor" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->no_telp_kantor; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Email Pribadi
          </span>
          <input type="text" class="form-control" placeholder="Email Pribadi" name="email_pribadi" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->email_pribadi; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Email Bisnis
          </span>
          <input type="text" class="form-control" placeholder="Email Bisnis" name="email_bisnis" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->email_bisnis; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Nama Institusi
          </span>
          <input type="text" class="form-control" placeholder="Nama Institusi" name="nama_institusi" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->nama_institusi; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Alamat Institusi
          </span>
          <input type="text" class="form-control" placeholder="Alamat Institusi" name="alamat_institusi" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->alamat_institusi; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Provinsi Institusi
          </span>
          <input type="text" class="form-control" placeholder="Provinsi Institusi" name="provinsi_institusi" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->provinsi_institusi; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Kota Institusi
          </span>
          <input type="text" class="form-control" placeholder="Kota Institusi" name="kota_institusi" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->kota_institusi; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Kode Pos Institusi
          </span>
          <input type="text" class="form-control" placeholder="Kode Pos Institusi" name="kode_pos_institusi" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->kode_pos_institusi; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Program Studi
          </span>
          <input type="text" class="form-control" placeholder="Program Studi" name="program_studi" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->program_studi; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Bidang Keahlian
          </span>
          <input type="text" class="form-control" placeholder="Bidang Keahlian" name="bidang_keahlian" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->bidang_keahlian; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Scopus ID
          </span>
          <input type="text" class="form-control" placeholder="Scopus ID" name="scopus_id" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->scopus_id; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Orcid ID
          </span>
          <input type="text" class="form-control" placeholder="Orcid ID" name="orcid_id" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->orcid_id; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Bidang Keahlian
          </span>
          <input type="text" class="form-control" placeholder="Bidang Keahlian" name="bidang_keahlian" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->bidang_keahlian; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Google Scholar ID
          </span>
          <input type="text" class="form-control" placeholder="Google Scholar ID" name="google_s_id" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->google_s_id; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Sinta ID
          </span>
          <input type="text" class="form-control" placeholder="Sinta ID" name="sinta_id" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->sinta_id; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Publon ID
          </span>
          <input type="text" class="form-control" placeholder="Publon ID" name="publon_id" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->publon_id; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Profesi
          </span>
          <input type="text" class="form-control" placeholder="Profesi" name="profesi" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->profesi; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Nama Jurnal
          </span>
          <input type="text" class="form-control" placeholder="Nama Jurnal" name="nama_jurnal" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->nama_jurnal; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            PIC Jurnal
          </span>
          <input type="text" class="form-control" placeholder="PIC Jurnal" name="pic" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->pic; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            ISSN
          </span>
          <input type="text" class="form-control" placeholder="ISSN" name="issn" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->issn; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
          URL Jurnal
          </span>
          <input type="text" class="form-control" placeholder="URL Jurnal" name="url_jurnal" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->url_jurnal; ?> " disabled>
        </div>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
          NIM
          </span>
          <input type="text" class="form-control" placeholder="NIM" name="nim" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->nim; ?> " disabled>
        </div>
        <?php if ($dataAnggota->profesi == 'mahasiswa') { ?>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
          KTM
          </span>
          <span class="form-control">
          <a href="assets/images/pendaftaran/<?php echo $dataAnggota->foto_ktm; ?>" class="btn btn-xs btn-success"> Tampilkan Gambar </a>
          </span>
        </div>
        <?php } ?>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
          Paket 
          </span>
          <input type="text" class="form-control" placeholder="Paket" name="administrasi_profesi" aria-describedby="sizing-addon2" value="<?php echo $dataAnggota->administrasi_profesi; ?> " disabled>
        </div>
        <p style="color:red;">Jika data sudah sesuai dan sudah menyelesaikan pembayaran, silahkan ubah status dibawah ini: </p>
        <br>
        <div class="input-group form-group">
          <span class="input-group-addon" id="sizing-addon2">
            Status
          </span>
          <select name="status" class="form-control select2"  aria-describedby="sizing-addon2">
            <?php
                if( $dataAnggota->status == 'non aktif'){
                  echo "<option value='non aktif' selected> Non aktif </option>";
                  echo "<option value='aktif'> Aktif </option>";
                  echo "<option value='masa tenggang'> Masa Tenggang </option>";
                }else if( $dataAnggota->status == 'aktif'){
                  echo "<option value='non aktif' > Non aktif </option>";
                  echo "<option value='aktif' selected> Aktif </option>";
                  echo "<option value='masa tenggang'> Masa Tenggang </option>";
                }else if( $dataAnggota->status == 'masa tenggang'){
                  echo "<option value='non aktif' > Non aktif </option>";
                  echo "<option value='aktif' > Aktif </option>";
                  echo "<option value='masa tenggang' selected> Masa Tenggang </option>";
                }
            ?>
          </select>
        </div>
        <div class="form-group">
          <div class="col-md-12">
              <button type="submit" class="form-control btn btn-primary"> <i class="glyphicon glyphicon-ok"></i> Accept Data</button>
          </div>
        </div>
      </form>
</div>

<script type="text/javascript">
$(function () {
    $(".select2").select2();

    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
});
</script>
