<?php
  foreach ($dataValidasianggota as $anggota) {
    ?>
    <tr>
      <td style="min-width:230px;"><?php echo $anggota->kode_anggota; ?></td>
      <td><?php echo $anggota->nama_lengkap; ?></td>
      <td><?php echo $anggota->tanggal_join; ?></td>
      <td><?php echo $anggota->tanggal_expired; ?></td>
      <td><?php echo $anggota->nama_institusi; ?></td>
      <td><?php echo $anggota->status; ?></td>
      <td class="text-center" style="min-width:230px;">
        <button class="btn btn-success update-dataValidasianggota" data-id="<?php echo $anggota->kode_anggota; ?>"><i class="glyphicon glyphicon-ok"></i> Accept</button>
        <button class="btn btn-danger konfirmasiHapus-validasianggota" data-id="<?php echo $anggota->kode_anggota; ?>" data-toggle="modal" data-target="#konfirmasiHapus"><i class="glyphicon glyphicon-remove-sign"></i> Delete</button>
      </td>
    </tr>
    <?php
  }
?>