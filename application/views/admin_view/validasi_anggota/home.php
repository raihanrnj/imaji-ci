<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="box">
  <div class="box-header">
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table id="list-data" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>Kode Anggota</th>
          <th>Nama</th>
          <th>Tanggal Join</th>
          <th>Tanggal Expired</th>
          <th>Institusi</th>
          <th>Status</th>
          <th style="text-align: center;">Aksi</th>
        </tr>
      </thead>
      <tbody id="data-validasianggota">
        
      </tbody>
    </table>
  </div>
</div>

<!-- <?php echo $modal_tambah_anggota; ?> -->

<div id="tempat-modal"></div>

<?php show_my_confirm('konfirmasiHapus', 'hapus-dataValidasianggota', 'Hapus Data Ini?', 'Ya, Hapus Data Ini'); ?>
<!-- <php
  $data['judul'] = 'Validasi Anggota';
  $data['url'] = 'Validasi_anggota/import';
  echo show_my_modal('admin_view/modals/modal_import', 'import-validasianggota', $data);
?> -->