<div class="msg" style="display:none;">
    <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="box">
    <div class="box-header">
        <div class="col-md-4">
            <button class="form-control btn btn-primary" data-toggle="modal" data-target="#modal-default"><i class="glyphicon glyphicon-plus-sign"></i> Tambah Data</button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="list-data" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Afiliasi</th>
                    <th>Image</th>
                    <th style="text-align: center;">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = $this->uri->segment('3') + 1;
                foreach ($pendiri as $s) : ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $s['nama'] ?></td>
                        <td><?= $s['alamat'] ?></td>
                        <td><?= $s['afiliasi'] ?></td>
                        <td><img class="img-fluid" style="width: 250px;" src="<?= base_url() ?>assets/images/pendiri/<?= $s['foto']; ?>" alt=""></td>
                        <td>
                            <a class="btn btn-warning" style="margin-top: 4px;" href="" data-toggle="modal" data-target="#modal-update_data<?= $s['id'] ?>"><i class="glyphicon glyphicon-edit"></i> </a>
                            <a class="btn btn-danger" style="margin-top: 4px;" href="" data-toggle="modal" data-target="#modal-delete_data<?= $s['id'] ?>"><i class="glyphicon glyphicon-trash"></i> </a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>


<!-- Modal Add data -->
<div class="modal fade" id="modal-default">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Data Pendiri</h4>
            </div>
            <?php echo form_open_multipart('user_pendiri/post_pendiri', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" placeholder="Masukan nama pendiri" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="judul" class="col-sm-2 control-label">Alamat Provinsi</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="alamat" name="alamat" required>
                                <option disabled value selected>Pilih Provinsi</option>
                                <?php
                                foreach ($provinsi as $row) {
                                    echo '<option value="' . $row->name . '">' . $row->name . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="afiliasi" class="col-sm-2 control-label">Afiliasi</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="afiliasi" placeholder="Masukan afiliasi pendiri" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama_gambar" class="col-sm-2 control-label">Foto Pendiri</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="nama_gambar" placeholder="" required>
                            <small class="label bg-aqua">Maximal 1MB</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Modal Update data -->
<?php
$no = $this->uri->segment('3') + 1;
foreach ($pendiri as $s) : ?>
    <div class="modal fade" id="modal-update_data<?= $s['id'] ?>">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Data Pendiri</h4>
                </div>
                <?php echo form_open_multipart('user_pendiri/pendiri_edit', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="nama" class="col-sm-2 control-label">Nama</label>
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" name="id" value="<?= $s['id']; ?>" required>
                                <input type="text" class="form-control" name="nama" placeholder="Masukan nama pendiri" value="<?= $s['nama']; ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="judul" class="col-sm-2 control-label">Alamat Provinsi</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="alamat">
                                    <option><?= $s['alamat']; ?></option>
                                    <?php
                                    foreach ($provinsi as $row2) {
                                        echo '<option value="' . $row2->name . '">' . $row2->name . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="afiliasi" class="col-sm-2 control-label">Afiliasi</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="afiliasi" placeholder="Masukan afiliasi pendiri" value="<?= $s['afiliasi']; ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nama_gambar" class="col-sm-2 control-label">Foto Pendiri</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" name="nama_gambar" placeholder="" value="<?= $s['foto'] ?>">
                                <small class=" label bg-aqua">Maximal 1MB</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach ?>

<!-- Modal hapus data -->
<?php
$no = $this->uri->segment('3') + 1;
foreach ($pendiri as $s) : ?>
    <div class="modal fade" id="modal-delete_data<?= $s['id'] ?>">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Hapus data ini ?</h4>
                </div>
                <?php echo form_open_multipart('user_pendiri/pendiri_delete', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6"><a href="<?= base_url() ?>user_pendiri/pendiri_delete/<?= $s['id'] ?>" class="form-control btn btn-primary"> <i class="glyphicon glyphicon-ok-sign"></i> Ya, Hapus Data Ini</a></div>
                            <div class="col-md-6"><button class="form-control btn btn-danger" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Tidak</button></div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach ?>

<div id="tempat-modal"></div>