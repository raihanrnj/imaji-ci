<div class="msg" style="display:none;">
    <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="box">
    <div class="box-header">
        <div class="col-md-4">
            <button class="form-control btn btn-primary" data-toggle="modal" data-target="#modal-default"><i class="glyphicon glyphicon-plus-sign"></i> Tambah Data</button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="list-data" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>isi / Content</th>
                    <th>Gambar</th>
                    <th style="text-align: center;">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = $this->uri->segment('3') + 1;
                foreach ($info_keanggotaan as $s) : ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $s['isi'] ?></td>
                        <td><img class="img-fluid" style="width: 250px;" src="<?= base_url() ?>assets/images/keanggotaan/<?= $s['gambar']; ?>" alt=""></td>
                        <td>
                            <a class=" btn btn-warning" style="margin-top: 4px;" href="" data-toggle="modal" data-target="#modal-update_data<?= $s['id'] ?>"><i class="glyphicon glyphicon-edit"></i> </a>
                            <a class="btn btn-danger" style="margin-top: 4px;" href="" data-toggle="modal" data-target="#modal-delete_data<?= $s['id'] ?>"><i class="glyphicon glyphicon-trash"></i> </a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>


<!-- Modal Add data -->
<div class="modal fade" id="modal-default">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Informasi Keanggotaan</h4>
            </div>
            <?php echo form_open_multipart('user_keanggotaan/info_keanggotaan_add', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group">
                        <label for="alamat" class="col-sm-2 control-label">Isi</label>
                        <div class="col-sm-10">
                            <textarea class="textarea" name="isi_keanggotaan" placeholder="Masukan content informasi keanggotaan " style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="no_hp" class="col-sm-2 control-label">Gambar</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="nama_gambar" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Modal Update data -->
<?php
$no = $this->uri->segment('3') + 1;
foreach ($info_keanggotaan as $k) : ?>
    <div class="modal fade" id="modal-update_data<?= $k['id'] ?>">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Informasi Keanggotaan</h4>
                </div>
                <?php echo form_open_multipart('user_keanggotaan/info_keanggotaan_edit', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="alamat" class="col-sm-2 control-label">Isi</label>
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" name="id" value="<?= $k['id'] ?>" required>
                                <textarea class="textarea" name="isi_keanggotaan" placeholder="Masukan content syarat keanggotaan " style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required><?= $k['isi'] ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="no_hp" class="col-sm-2 control-label">Gambar</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" name="nama_gambar">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach ?>

<!-- Modal hapus data -->
<?php
$no = $this->uri->segment('3') + 1;
foreach ($info_keanggotaan as $i) : ?>
    <div class="modal fade" id="modal-delete_data<?= $i['id'] ?>">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Hapus data ini ?</h4>
                </div>
                <?php echo form_open_multipart('user_keanggotaan/info_keanggotaan_delete', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6"><a href="<?= base_url() ?>user_keanggotaan/info_keanggotaan_delete/<?= $i['id'] ?>" class="form-control btn btn-primary"> <i class="glyphicon glyphicon-ok-sign"></i> Ya, Hapus Data Ini</a></div>
                            <div class="col-md-6"><button class="form-control btn btn-danger" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Tidak</button></div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach ?>