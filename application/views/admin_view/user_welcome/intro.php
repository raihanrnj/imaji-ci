<div class="msg" style="display:none;">
    <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="box">
    <div class="box-header">
        <div class="col-md-4">
            <button class="form-control btn btn-primary" data-toggle="modal" data-target="#modal-default"><i class="glyphicon glyphicon-plus-sign"></i> Tambah Data</button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="list-data" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Judul</th>
                    <th>isi</th>
                    <th>Gambar</th>
                    <th style="text-align: center;">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = $this->uri->segment('3') + 1;
                foreach ($intro as $s) : ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $s['judul'] ?></td>
                        <td><?= $s['isi'] ?></td>
                        <td><img class="img-fluid" style="width: 250px;" src="<?= base_url() ?>assets/images/intro/<?= $s['gambar']; ?>" alt=""></td>
                        <td>
                            <!-- <a class="btn btn-warning" style="margin-top: 4px;" href="" data-toggle="modal" data-target="#modal-update_data<?= $s['id'] ?>"><i class="glyphicon glyphicon-repeat"></i> Update</a>
                            <a class="btn btn-danger" style="margin-top: 4px;" href="" data-toggle="modal" data-target="#modal-delete_data<?= $s['id'] ?>"><i class="glyphicon glyphicon-remove-sign"></i> Delete</a> -->
                            <a class="btn btn-warning" style="margin-top: 4px;" href="" data-toggle="modal" data-target="#modal-update_data<?= $s['id'] ?>"><i class="glyphicon glyphicon-edit"></i> </a>
                            <a class="btn btn-danger" style="margin-top: 4px;" href="" data-toggle="modal" data-target="#modal-delete_data<?= $s['id'] ?>"><i class="glyphicon glyphicon-trash"></i> </a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>


<!-- Modal Add data -->
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Intro</h4>
            </div>
            <?php echo form_open_multipart('user_welcome/intro_add', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group">
                        <label for="judul" class="col-sm-2 control-label">Judul</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="judul" placeholder="Masukan judul slider" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sub_judul" class="col-sm-2 control-label">Isi</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="isi" cols="30" rows="4" placeholder="Masukan isi intro / content"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama_gambar" class="col-sm-2 control-label">Gambar</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="nama_gambar" placeholder="">
                            <small class="label bg-aqua">Maximal 1MB</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Modal Update data -->
<?php
$no = $this->uri->segment('3') + 1;
foreach ($intro as $s) : ?>
    <div class="modal fade" id="modal-update_data<?= $s['id'] ?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Intro</h4>
                </div>
                <?php echo form_open_multipart('user_welcome/intro_edit', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Judul</label>
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" name="id" placeholder="Masukan judul Intro" value="<?= $s['id'] ?>" required>
                                <input type="text" class="form-control" name="judul" placeholder="Masukan judul Intro" value="<?= $s['judul'] ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Isi</label>
                            <div class="col-sm-10">
                                <textarea name="isi" class="form-control" cols="30" rows="5" placeholder="Masukan isi intro / content" value="<?= $s['isi'] ?>"><?= $s['isi'] ?></textarea>
                                <!-- <input type="text" class="form-control" name="isi" placeholder="Masukan isi intro / content" value="<?= $s['isi'] ?>"> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Gambar</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" name="nama_gambar" placeholder="" value="<?= $s['gambar'] ?>">
                                <small class="label bg-aqua">Maximal 1MB</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach ?>

<!-- Modal hapus data -->
<?php
$no = $this->uri->segment('3') + 1;
foreach ($intro as $s) : ?>
    <div class="modal fade" id="modal-delete_data<?= $s['id'] ?>">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Hapus data ini ?</h4>
                </div>
                <?php echo form_open_multipart('user_welcome/intro_edit', 'class="form-horizontal"', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6"><a href="<?= base_url() ?>user_welcome/intro_delete/<?= $s['id'] ?>" class="form-control btn btn-primary"> <i class="glyphicon glyphicon-ok-sign"></i> Ya, Hapus Data Ini</a></div>
                            <div class="col-md-6"><button class="form-control btn btn-danger" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Tidak</button></div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach ?>

<div id="tempat-modal"></div>