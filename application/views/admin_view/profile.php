<div class="row">
  <div class="col-md-3">
    <!-- Profile Image -->
    <div class="box box-primary">
      <div class="box-body box-profile">
        <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url(); ?>assets/images/pendaftaran/<?php echo $userdata->pas_foto; ?>" alt="User profile picture">

        <h3 class="profile-username text-center"><?php echo $userdata->nama_lengkap; ?></h3>
        <h3 class="profile-username text-center"><?php echo $userdata->kode_anggota; ?></h3>

        <p class="text-muted text-center"><?php echo $userdata->profesi; ?></p>

        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>Username</b> <a class="pull-right"><?php echo $userdata->email_pribadi; ?></a>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="col-md-9">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Settings</a></li>
        <li><a href="#password" data-toggle="tab">Ubah Password</a></li>
      </ul>
      <div class="tab-content">
        <div class="active tab-pane" id="settings">
          <form class="form-horizontal" action="<?php echo base_url('Profile/update') ?>" method="POST" enctype="multipart/form-data">
            <div class="form-group">
              <label for="inputUsername" class="col-sm-2 control-label">Kode Anggota</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id= placeholder="Kode Anggota" name="email_pribadi" value="<?php echo $userdata->kode_anggota; ?>" disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputUsername" class="col-sm-2 control-label">Email</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id= placeholder="Email" name="email_pribadi" value="<?php echo $userdata->email_pribadi; ?>">
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">Name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Name" name="nama_lengkap" value="<?php echo $userdata->nama_lengkap; ?>">
              </div>
            </div>
            <div class="form-group">
              <label for="inputFoto" class="col-sm-2 control-label">Foto</label>
              <div class="col-sm-10">
                <input type="file" class="form-control" placeholder="Foto" name="pas_foto">
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">Status</label>
              <div class="col-sm-10">
              <?php if ($userdata->status == 'aktif') { ?>
              <a href="#" class="btn btn-xs btn-success"> <?php echo $userdata->status; ?> </a>
              <?php } else  { ?>
                <a href="#" class="btn btn-xs btn-warning"> <?php echo $userdata->status; ?> </a>
                <p style="color:red;"> *Pastikan anda sudah melakukan pembayaran uang keanggotaan. <br> 
                Akun akan aktif setelah team IMAJI menvalidasi pembayaran.</p>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">Tanggal Join</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Tanggal Join" name="tanggal_join" value="<?php echo $userdata->tanggal_join; ?>" disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">Tanggal Expired</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Tanggal Expired" name="tanggal_expired" value="<?php echo $userdata->tanggal_expired; ?>" disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Tempat Lahir
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Tempat Lahir" name="tempat_lahir" value="<?php echo $userdata->tempat_lahir; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Tanggal Lahir
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Tanggal Lahir" name="tanggal_lahir" value="<?php echo $userdata->tanggal_lahir; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              NIK
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="NIk" name="no_nik" value="<?php echo $userdata->no_nik; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
            <label for="inputNama" class="col-sm-2 control-label">
            KTP
              </label>
              <div class="col-sm-10">
                <span class="form-control">
                <a href="assets/images/pendaftaran/<?php echo $userdata->kartu_identitas; ?>" class="btn btn-xs btn-success"> Tampilkan Gambar </a>
                </label>
                </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Alamat
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Alamat" name="alamat" value="<?php echo $userdata->alamat; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Provinsi
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Provinsi" name="provinsi" value="<?php echo $userdata->provinsi; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Kota
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Kota" name="kota" value="<?php echo $userdata->kota; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Kode Pos
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Kode Pos" name="kode_pos" value="<?php echo $userdata->kode_pos; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Nomer HP
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Nomer HP" name="no_hp" value="<?php echo $userdata->no_hp; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              No Telp Kantor
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Nomer Telp Kantor" name="no_telp_kantor" value="<?php echo $userdata->no_telp_kantor; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Email Pribadi
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Email Pribadi" name="email_pribadi" value="<?php echo $userdata->email_pribadi; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Email Bisnis
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Email Bisnis" name="email_bisnis" value="<?php echo $userdata->email_bisnis; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Nama Institusi
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Nama Institusi" name="nama_institusi" value="<?php echo $userdata->nama_institusi; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Alamat Institusi
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Alamat Institusi" name="alamat_institusi" value="<?php echo $userdata->alamat_institusi; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Provinsi Institusi
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Provinsi Institusi" name="provinsi_institusi" value="<?php echo $userdata->provinsi_institusi; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Kota Institusi
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Kota Institusi" name="kota_institusi" value="<?php echo $userdata->kota_institusi; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Kode Pos Institusi
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Kode Pos Institusi" name="kode_pos_institusi" value="<?php echo $userdata->kode_pos_institusi; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Program Studi
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Program Studi" name="program_studi" value="<?php echo $userdata->program_studi; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Bidang Keahlian
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Bidang Keahlian" name="bidang_keahlian" value="<?php echo $userdata->bidang_keahlian; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Scopus ID
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Scopus ID" name="scopus_id" value="<?php echo $userdata->scopus_id; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Orcid ID
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Orcid ID" name="orcid_id" value="<?php echo $userdata->orcid_id; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Bidang Keahlian
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Bidang Keahlian" name="bidang_keahlian" value="<?php echo $userdata->bidang_keahlian; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Google Scholar ID
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Google Scholar ID" name="google_s_id" value="<?php echo $userdata->google_s_id; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Sinta ID
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Sinta ID" name="sinta_id" value="<?php echo $userdata->sinta_id; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Publon ID
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Publon ID" name="publon_id" value="<?php echo $userdata->publon_id; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Profesi
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Profesi" name="profesi" value="<?php echo $userdata->profesi; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Nama Jurnal
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Nama Jurnal" name="nama_jurnal" value="<?php echo $userdata->nama_jurnal; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              PIC Jurnal
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="PIC Jurnal" name="pic" value="<?php echo $userdata->pic; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              ISSN
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="ISSN" name="issn" value="<?php echo $userdata->issn; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              URL Jurnal
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="URL Jurnal" name="url_jurnal" value="<?php echo $userdata->url_jurnal; ?> " disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              NIM
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="NIM" name="nim" value="<?php echo $userdata->nim; ?> " disabled>
              </div>
            </div>
            <?php if ($userdata->profesi == 'mahasiswa') { ?>
            <div class="form-group">
            <label for="inputNama" class="col-sm-2 control-label">
            KTM
            </label>
            <div class="col-sm-10">
              <span class="form-control">
              <a href="assets/images/pendaftaran/<?php echo $userdata->foto_ktm; ?>" class="btn btn-xs btn-success"> Tampilkan Gambar </a>
              </label>
                </div>
            </div>
            <?php } ?>
            <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">
              Paket 
              </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Paket" name="administrasi_profesi" value="<?php echo $userdata->administrasi_profesi; ?> " disabled>
              </div>
            </div>  

            
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-danger">Submit</button>
              </div>
            </div>
          </form>
        </div>
        <div class="tab-pane" id="password">
          <form class="form-horizontal" action="<?php echo base_url('Profile/ubah_password') ?>" method="POST">
            <div class="form-group">
              <label for="passLama" class="col-sm-2 control-label">Password Lama</label>
              <div class="col-sm-10">
                <input type="password" class="form-control" placeholder="Password Lama" name="passLama">
              </div>
            </div>
            <div class="form-group">
              <label for="passBaru" class="col-sm-2 control-label">Password Baru</label>
              <div class="col-sm-10">
                <input type="password" class="form-control" placeholder="Password Baru" name="passBaru">
              </div>
            </div>
            <div class="form-group">
              <label for="passKonf" class="col-sm-2 control-label">Konfirmasi Password</label>
              <div class="col-sm-10">
                <input type="password" class="form-control" placeholder="Konfirmasi Password" name="passKonf">
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-danger">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="msg" style="display:none;">
      <?php echo $this->session->flashdata('msg'); ?>
    </div>
  </div>
</div>