<!-- REQUIRED JS SCRIPTS -->
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>assets/assets_admin/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets_admin/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets_admin/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets_admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets_admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- code editor-->
<script src="<?php echo base_url(); ?>assets/assets_admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/assets_admin/dist/js/app.min.js"></script>

<!-- My Ajax -->
<?php include './assets/assets_admin/js/ajax.php'; ?>