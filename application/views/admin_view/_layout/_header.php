<header class="main-header">
  <!-- Logo -->
  <a href="<?php echo base_url(); ?>" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><small>IMAJI</small></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Admin</b>IMAJI</span>
  </a>
  <link rel="shortcut icon" type="image/jpg" href="<?= base_url() ?>assets/images/logo.png"/>
  <!-- nav -->
  <?php echo @$_nav; ?>
</header>