<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url(); ?>assets/images/pendaftaran/<?php echo $userdata->pas_foto; ?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $userdata->nama_lengkap; ?></p>
        <!-- Status -->
        <a href="<?php echo base_url(); ?>assets/assets_admin/#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
      <li class="header">LIST MENU</li>
      <!-- Optionally, you can add icons to the links -->

      <li <?php if ($page == 'home') {
            echo 'class="active"';
          } ?>>
        <a href="<?php echo base_url('Home'); ?>">
          <i class="fa fa-home"></i>
          <span>Home</span>
        </a>
      </li>

      <li <?php if ($page == 'profile') {
            echo 'class="active"';
          } ?>>
        <a href="<?php echo base_url('Profile'); ?>">
          <i class="fa fa-home"></i>
          <span>Profile</span>
        </a>
      </li>

      <?php if ($userdata->level == 'admin') { ?>



        <li <?php if ($page == 'anggota') {
              echo 'class="active"';
            } ?>>
          <a href="<?php echo base_url('Anggota'); ?>">
            <i class="fa fa-user"></i>
            <span>Data Anggota</span>
          </a>
        </li>
        <li <?php if ($page == 'validasi_anggota') {
              echo 'class="active"';
            } ?>>
          <a href="<?php echo base_url('Validasi_anggota'); ?>">
            <i class="fa fa-user"></i>
            <span>Validasi Calon Anggota</span>
          </a>
        </li>

        <li <?php if (trim($page == 'slider') || $page == 'intro' || $page == 'galeri') {
              echo 'class="active"';
            } ?>>
          <a href="#">
            <i class="fa fa-file-text" aria-hidden="true"></i> <span>Welcome Page</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if ($page == 'slider') {
                  echo 'class="active"';
                } ?>><a href="<?= base_url() ?>user_welcome/slider"><i class="fa fa-circle-o"></i> Slider</a></li>
            <li <?php if ($page == 'intro') {
                  echo 'class="active"';
                } ?>><a href="<?= base_url() ?>user_welcome/intro"><i class="fa fa-circle-o"></i> Intro</a></li>
            <li <?php if ($page == 'galeri') {
                  echo 'class="active"';
                } ?>><a href="<?= base_url() ?>user_welcome/galeri"><i class="fa fa-circle-o"></i> Galeri</a></li>
          </ul>
        </li>


        <li <?php if (trim($page == 'pengantar') || $page == 'akta_pendirian' || $page == 'lambang_dan_arti' || $page == 'visi_misi' || $page == 'kode_etik') {
              echo 'class="active"';
            } ?>>
          <a href="#">
            <i class="fa fa-user" aria-hidden="true"></i> <span>Profile Page</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if ($page == 'pengantar') {
                  echo 'class="active"';
                } ?>><a href="<?= base_url() ?>user_profiles/pengantar"><i class="fa fa-circle-o"></i> Pengantar</a></li>
            <li <?php if ($page == 'akta_pendirian') {
                  echo 'class="active"';
                } ?>><a href="<?= base_url() ?>user_profiles/akta_pendirian"><i class="fa fa-circle-o"></i> Akta Pendirian</a></li>
            <li <?php if ($page == 'lambang_dan_arti') {
                  echo 'class="active"';
                } ?>><a href="<?= base_url() ?>user_profiles/lambang_dan_arti"><i class="fa fa-circle-o"></i> Lambang dan Arti</a></li>
            <li <?php if ($page == 'visi_misi') {
                  echo 'class="active"';
                } ?>><a href="<?= base_url() ?>user_profiles/visi_misi"><i class="fa fa-circle-o"></i> Visi dan Misi</a></li>
            <li <?php if ($page == 'kode_etik') {
                  echo 'class="active"';
                } ?>><a href="<?= base_url() ?>user_profiles/kode_etik"><i class="fa fa-circle-o"></i> Kode Etik</a></li>
          </ul>
        </li>

        <li <?php if (trim($page == 'pendiri') || $page == 'pengurus' || $page == 'desc_pendiri' || $page == 'desc_pengurus') {
              echo 'class="active"';
            } ?>>
          <a href="#">
            <i class="fa fa-users"></i> <span>Organisasi Page</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if (trim($page == 'desc_pendiri') || $page == 'desc_pengurus') {
                  echo 'class="active"';
                } ?>>
              <a href="#"><i class="fa fa-circle-o"></i> Deskripsi
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li <?php if ($page == 'desc_pendiri') {
                      echo 'class="active"';
                    } ?>><a href="<?= base_url() ?>user_pendiri/description"><i class="fa fa-circle-o"></i> Halaman Pendiri</a></li>
                <li <?php if ($page == 'desc_pengurus') {
                      echo 'class="active"';
                    } ?>><a href="<?= base_url() ?>user_pengurus/description"><i class="fa fa-circle-o"></i> Halaman Pengurus</a></li>
              </ul>
            </li>
            <li <?php if (trim($page == 'pendiri') || $page == 'pengurus') {
                  echo 'class="active"';
                } ?>>
              <a href="#"><i class="fa fa-circle-o"></i> Data
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li <?php if ($page == 'pendiri') {
                      echo 'class="active"';
                    } ?>><a href="<?= base_url() ?>user_pendiri/"><i class="fa fa-circle-o"></i> Data Pendiri</a></li>
                <li <?php if ($page == 'pengurus') {
                      echo 'class="active"';
                    } ?>><a href="<?= base_url() ?>user_pengurus/"><i class="fa fa-circle-o"></i> Data Pengurus</a></li>
              </ul>
            </li>
          </ul>
        </li>

        <li <?php if ($page == 'berita') {
              echo 'class="active"';
            } ?>>
          <a href="<?php echo base_url('Post_berita'); ?>">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
            <span>Berita Page</span>
          </a>
        </li>

        <li <?php if (trim($page == 'keanggotaan') || $page == 'info_keanggotaan') {
              echo 'class="active"';
            } ?>>
          <a href="#">
            <i class="fa fa-users" aria-hidden="true"></i> <span>Keanggotaan Page</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if ($page == 'keanggotaan') {
                  echo 'class="active"';
                } ?>><a href="<?= base_url() ?>user_keanggotaan/keanggotaan"><i class="fa fa-circle-o"></i> Syarat Keanggotaan</a></li>
            <li <?php if ($page == 'info_keanggotaan') {
                  echo 'class="active"';
                } ?>><a href="<?= base_url() ?>user_keanggotaan/info_keanggotaan"><i class="fa fa-circle-o"></i> Informasi Keanggotaan</a></li>
          </ul>
        </li>
        <li <?php if ($page == 'jurnal') {
              echo 'class="active"';
            } ?>>
          <a href="<?php echo base_url('user_jurnal'); ?>">
            <i class="fa fa-book" aria-hidden="true"></i>
            <span>Jurnal</span>
          </a>
        </li>
        <li <?php if ($page == 'contact') {
              echo 'class="active"';
            } ?>>
          <a href="<?php echo base_url('user_contact'); ?>">
            <i class="fa fa-phone-square" aria-hidden="true"></i>
            <span>Kontak Page</span>
          </a>
        </li>
      <?php } ?>
    </ul>
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>