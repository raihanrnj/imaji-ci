<?php
defined('BASEPATH') or exit('No direct script access allowed');
include_once(APPPATH . 'core/AUTH_Controller.php');

class post_berita extends AUTH_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_berita');
        $this->load->library('upload');
    }
    function index()
    {
        $data['userdata']     = $this->userdata;

        $data['page']         = "berita";
        $data['judul']         = "Berita";
        $data['deskripsi']     = "Manage Berita";

        $data['berita'] = $this->M_berita->get_berita_all();
        $this->template->views('admin_view/berita/berita', $data);
    }

    function post_berita()
    {
        $judul        = $this->input->post('judul');
        $berita    = $this->input->post('berita');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('berita', 'Berita', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/images/berita';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'berita_judul'      => $judul,
                        'berita_isi' => $berita,
                        'berita_image'      => $nama_gambar['file_name'],
                        'berita_tanggal' => date('Y-m-d H:i:s'),
                    );
                    $this->M_berita->insert_berita($data);
                    $this->session->set_flashdata('msg', show_succ_msg('Berita Berhasil ditambahkan', '20px'));
                    redirect('post_berita');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Berita Gagal ditambahkan', '20px'));
                    redirect('post_berita');
                }
            }
        }
    }

    public function berita_edit()
    {
        $berita_id = $this->input->post('berita_id');
        $judul        = $this->input->post('judul');
        $berita    = $this->input->post('berita');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('berita', 'Berita', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('berita_id' => $berita_id);

            $config['upload_path']   = './assets/images/berita';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);
            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'berita_judul'      => $judul,
                        'berita_isi' => $berita,
                        'berita_image'      => $nama_gambar['file_name'],
                        'berita_tanggal' => date('Y-m-d H:i:s'),
                    );
                    $this->M_berita->update_berita($data, $data_id);
                    $this->session->set_flashdata('msg', show_succ_msg('Berita Berhasil diupdate', '20px'));
                    redirect('post_berita');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Berita Gagal diupdate', '20px'));
                    redirect('post_berita');
                }
            } else {
                $data = array(
                    'berita_judul'      => $judul,
                    'berita_isi' => $berita,
                    'berita_tanggal' => date('Y-m-d H:i:s'),
                );
                $this->M_berita->update_berita($data, $data_id);
                $this->session->set_flashdata('msg', show_succ_msg('Berita Berhasil diupdate', '20px'));
                redirect('post_berita');
            }
        }
    }

    public function berita_delete($id)
    {
        $where = array('berita_id' => $id);
        $this->M_berita->delete_berita($where, 'berita');
        $this->session->set_flashdata('msg', show_succ_msg('Berita Berhasil dihapus', '20px'));
        redirect('post_berita');
    }
}
