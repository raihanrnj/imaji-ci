<?php
defined('BASEPATH') or exit('No direct script access allowed');
include_once(APPPATH . 'core/AUTH_Controller.php');

class User_jurnal extends AUTH_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_jurnal');
        $this->load->library('upload');
    }

    function index()
    {
        $data['userdata']     = $this->userdata;

        $data['page']         = "jurnal";
        $data['judul']         = "Jurnal";
        $data['deskripsi']     = "Manage Jurnal";

        $data['jurnal'] = $this->M_jurnal->get_jurnal_all();
        $this->template->views('admin_view/jurnal/home', $data);
    }

    function post_jurnal()
    {
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');
        $link_jurnal        = $this->input->post('link_jurnal');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
        $this->form_validation->set_rules('link_jurnal', 'Link Jurnal', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/images/jurnal';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'jurnal_judul'      => $judul,
                        'jurnal_deskripsi' => $deskripsi,
                        'jurnal_link' => $link_jurnal,
                        'jurnal_image'      => $nama_gambar['file_name'],
                        'jurnal_tanggal' => date('Y-m-d H:i:s'),
                    );
                    $this->M_jurnal->insert_jurnal($data);
                    $this->session->set_flashdata('msg', show_succ_msg('Jurnal Berhasil ditambahkan', '20px'));
                    redirect('user_jurnal');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Jurnal Gagal ditambahkan', '20px'));
                    redirect('user_jurnal');
                }
            }
        }
    }

    public function jurnal_edit()
    {
        $jurnal_id = $this->input->post('jurnal_id');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');
        $link_jurnal        = $this->input->post('link_jurnal');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('link_jurnal', 'Link Jurnal', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('jurnal_id' => $jurnal_id);

            $config['upload_path']   = './assets/images/jurnal';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);
            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'jurnal_judul'      => $judul,
                        'jurnal_deskripsi' => $deskripsi,
                        'jurnal_link' => $link_jurnal,
                        'jurnal_image'      => $nama_gambar['file_name'],
                        'jurnal_tanggal' => date('Y-m-d H:i:s'),
                    );
                    $this->M_jurnal->update_jurnal($data, $data_id);
                    $this->session->set_flashdata('msg', show_succ_msg('Jurnal Berhasil diupdate', '20px'));
                    redirect('user_jurnal');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Jurnal Gagal diupdate', '20px'));
                    redirect('user_jurnal');
                }
            } else {
                $data = array(
                    'jurnal_judul'      => $judul,
                    'jurnal_deskripsi' => $deskripsi,
                    'jurnal_link' => $link_jurnal,
                    'jurnal_tanggal' => date('Y-m-d H:i:s'),
                );
                $this->M_jurnal->update_jurnal($data, $data_id);
                $this->session->set_flashdata('msg', show_succ_msg('Jurnal Berhasil diupdate', '20px'));
                redirect('user_jurnal');
            }
        }
    }

    public function jurnal_delete($id)
    {
        $where = array('jurnal_id' => $id);
        $this->M_jurnal->delete_jurnal($where, 'jurnal');
        $this->session->set_flashdata('msg', show_succ_msg('Jurnal Berhasil dihapus', '20px'));
        redirect('user_jurnal');
    }
}
