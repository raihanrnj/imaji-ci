<?php
defined('BASEPATH') or exit('No direct script access allowed');
include_once(APPPATH . 'core/AUTH_Controller.php');

class User_keanggotaan extends AUTH_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_user_keanggotaan');
        $this->load->library('upload');
    }

    public function index()
    {
        redirect('user_keanggotaan/keanggotaan');
    }

    // Controller section Syarat Keanggotaan start
    public function keanggotaan()
    {
        $data['userdata']     = $this->userdata;

        $data['page']         = "keanggotaan";
        $data['judul']         = "Keanggotaan";
        $data['deskripsi']     = "Manage Keanggotaan";

        $data['keanggotaan'] = $this->M_user_keanggotaan->get_keanggotaan();
        $this->template->views('admin_view/user_keanggotaan/home', $data);
    }

    public function syarat_add()
    {
        $isi_keanggotaan    = $this->input->post('isi_keanggotaan');

        $this->form_validation->set_rules('isi_keanggotaan', 'Content Syarat Keanggotaan', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/images/keanggotaan';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'isi' => $isi_keanggotaan,
                        'gambar'      => $nama_gambar['file_name'],
                    );
                    $this->M_user_keanggotaan->insert($data);
                    $this->session->set_flashdata('msg', show_succ_msg('Syarat keanggotaan Berhasil ditambahkan', '20px'));
                    redirect('user_keanggotaan/keanggotaan');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Syarat keanggotaan Gagal ditambahkan', '20px'));
                    redirect('user_keanggotaan/keanggotaan');
                }
            }
        }
    }

    public function syarat_edit()
    {
        $id = $this->input->post('id');
        $isi_keanggotaan    = $this->input->post('isi_keanggotaan');

        $this->form_validation->set_rules('isi_keanggotaan', 'Content Syarat Keanggotaan', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/images/keanggotaan';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);
            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'isi' => $isi_keanggotaan,
                        'gambar'      => $nama_gambar['file_name'],
                    );
                    $this->M_user_keanggotaan->update($data, $data_id);
                    $this->session->set_flashdata('msg', show_succ_msg('Syarat Keanggotaan Berhasil ditambahkan', '20px'));
                    redirect('user_keanggotaan/keanggotaan');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Syarat Keanggotaan Gagal ditambahkan', '20px'));
                    redirect('user_keanggotaan/keanggotaan');
                }
            } else {
                $data = array(
                    'isi' => $isi_keanggotaan,
                );
                $this->M_user_keanggotaan->update($data, $data_id);
                $this->session->set_flashdata('msg', show_succ_msg('Syarat Keanggotaan Berhasil ditambahkan', '20px'));
                redirect('user_keanggotaan/keanggotaan');
            }
        }
    }

    public function syarat_delete($id)
    {
        $where = array('id' => $id);
        $this->M_user_keanggotaan->delete_keanggotaan($where, 'u_keanggotaan_syarat');
        $this->session->set_flashdata('msg', show_succ_msg('Syarat Keanggotaan Berhasil dihapus', '20px'));
        redirect('user_keanggotaan/keanggotaan');
    }
    // Controller section Syarat Keanggotaan end


    // Controller section Info_keanggotaan Start
    public function info_keanggotaan()
    {
        $data['userdata']     = $this->userdata;

        $data['page']         = "info_keanggotaan";
        $data['judul']         = "Info Keanggotaan";
        $data['deskripsi']     = "Manage Info Keanggotaan";

        $data['info_keanggotaan'] = $this->M_user_keanggotaan->get_info_keanggotaan();
        $this->template->views('admin_view/user_keanggotaan/info_keanggotaan', $data);
    }

    public function info_keanggotaan_add()
    {
        $isi_keanggotaan    = $this->input->post('isi_keanggotaan');

        $this->form_validation->set_rules('isi_keanggotaan', 'Info Keanggotaan', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/images/keanggotaan';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'isi' => $isi_keanggotaan,
                        'gambar'      => $nama_gambar['file_name'],
                    );
                    $this->M_user_keanggotaan->insert_info_keanggotaan($data);
                    $this->session->set_flashdata('msg', show_succ_msg('Infomasi keanggotaan Berhasil ditambahkan', '20px'));
                    redirect('user_keanggotaan/info_keanggotaan');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Infomasi keanggotaan Gagal ditambahkan', '20px'));
                    redirect('user_keanggotaan/info_keanggotaan');
                }
            }
        }
    }

    public function info_keanggotaan_edit()
    {
        $id = $this->input->post('id');
        $isi_keanggotaan    = $this->input->post('isi_keanggotaan');

        $this->form_validation->set_rules('isi_keanggotaan', 'Content Info Keanggotaan', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/images/keanggotaan';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);
            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'isi' => $isi_keanggotaan,
                        'gambar'      => $nama_gambar['file_name'],
                    );
                    $this->M_user_keanggotaan->update_info_keanggotaan($data, $data_id);
                    $this->session->set_flashdata('msg', show_succ_msg('Informasi Keanggotaan Berhasil ditambahkan', '20px'));
                    redirect('user_keanggotaan/info_keanggotaan');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Informasi Keanggotaan Gagal ditambahkan', '20px'));
                    redirect('user_keanggotaan/info_keanggotaan');
                }
            } else {
                $data = array(
                    'isi' => $isi_keanggotaan,
                );
                $this->M_user_keanggotaan->update_info_keanggotaan($data, $data_id);
                $this->session->set_flashdata('msg', show_succ_msg('Informasi Keanggotaan Berhasil ditambahkan', '20px'));
                redirect('user_keanggotaan/info_keanggotaan');
            }
        }
    }

    public function info_keanggotaan_delete($id)
    {
        $where = array('id' => $id);
        $this->M_user_keanggotaan->delete_info_keanggotaan($where, 'u_keanggotaan_pendaftaran');
        $this->session->set_flashdata('msg', show_succ_msg('Informasi Keanggotaan Berhasil dihapus', '20px'));
        redirect('user_keanggotaan/info_keanggotaan');
    }
}
