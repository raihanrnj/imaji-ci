<?php
defined('BASEPATH') or exit('No direct script access allowed');
include_once(APPPATH . 'core/AUTH_Controller.php');

class User_welcome extends AUTH_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_user_welcome');
        $this->load->library('upload');
    }

    public function index()
    {
        redirect('user_welcome/slider');
    }

    // Controller section slider start
    public function slider()
    {
        $data['userdata']     = $this->userdata;

        $data['page']         = "slider";
        $data['judul']         = "Slider";
        $data['deskripsi']     = "Manage Slider";

        // $data = array('sliders' => $this->M_user_welcome->M_user_welcome());
        $data['sliders'] = $this->M_user_welcome->get_slider();
        $this->template->views('admin_view/user_welcome/slider', $data);
    }

    public function slider_add()
    {
        $judul        = $this->input->post('judul');
        $sub_judul    = $this->input->post('sub_judul');
        $urutan    = $this->input->post('urutan');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('sub_judul', 'Sub Judul', 'trim|required');
        $this->form_validation->set_rules('urutan', 'urutan', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/images/slider';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'sub_judul' => $sub_judul,
                        'nama_gambar'      => $nama_gambar['file_name'],
                        'urutan'      => $urutan,
                    );
                    $this->M_user_welcome->insert($data);
                    // $this->session->set_flashdata('msg', '<div class="col-lg-14"><div class="alert alert-dismissable alert-danger">
                    // <button type="button" class="close" data-dismiss="alert">&times;</button>
                    // <strong>Upload Gagal!</strong>.</div></div>');
                    $this->session->set_flashdata('msg', show_succ_msg('Slider Berhasil ditambahkan', '20px'));
                    redirect('user_welcome/slider');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Slider Gagal ditambahkan', '20px'));
                    redirect('user_welcome/slider');
                }
            }
        }
    }

    public function slider_edit()
    {
        $id = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $sub_judul    = $this->input->post('sub_judul');
        $urutan    = $this->input->post('urutan');
        $link    = $this->input->post('link');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('sub_judul', 'Sub Judul', 'trim|required');
        $this->form_validation->set_rules('urutan', 'urutan', 'trim|required');
        $this->form_validation->set_rules('link', 'link', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/images/slider';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);
            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'sub_judul' => $sub_judul,
                        'nama_gambar'      => $nama_gambar['file_name'],
                        'urutan'      => $urutan,
                        'link'      => $link,
                    );
                    $this->M_user_welcome->update($data, $data_id);
                    $this->session->set_flashdata('msg', show_succ_msg('Slider Berhasil ditambahkan', '20px'));
                    redirect('user_welcome/slider');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Slider Gagal ditambahkan', '20px'));
                    redirect('user_welcome/slider');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'sub_judul' => $sub_judul,
                    'urutan' => $urutan,
                    'link' => $link,
                );
                $this->M_user_welcome->update($data, $data_id);
                $this->session->set_flashdata('msg', show_succ_msg('Slider Berhasil ditambahkan', '20px'));
                redirect('user_welcome/slider');
            }
        }
    }

    public function slider_delete($id)
    {
        $where = array('id' => $id);
        $this->M_user_welcome->delete_slider($where, 'u_welcome_slider');
        $this->session->set_flashdata('msg', show_succ_msg('Slider Berhasil dihapus', '20px'));
        redirect('user_welcome/slider');
    }
    // Controller section slider end


    // Controller section Intro Start
    public function intro()
    {
        $data['userdata']     = $this->userdata;

        $data['page']         = "intro";
        $data['judul']         = "Intro";
        $data['deskripsi']     = "Manage Intro";

        $data['intro'] = $this->M_user_welcome->get_intro();
        $this->template->views('admin_view/user_welcome/intro', $data);
    }

    public function intro_add()
    {
        $judul        = $this->input->post('judul');
        $isi    = $this->input->post('isi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('isi', 'Content', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/images/intro';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'isi' => $isi,
                        'gambar'      => $nama_gambar['file_name'],
                    );
                    $this->M_user_welcome->insert_intro($data);
                    // $this->session->set_flashdata('msg', '<div class="col-lg-14"><div class="alert alert-dismissable alert-danger">
                    // <button type="button" class="close" data-dismiss="alert">&times;</button>
                    // <strong>Upload Gagal!</strong>.</div></div>');
                    $this->session->set_flashdata('msg', show_succ_msg('Intro Berhasil ditambahkan', '20px'));
                    redirect('user_welcome/intro');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Intro Gagal ditambahkan', '20px'));
                    redirect('user_welcome/intro');
                }
            }
        }
    }

    public function intro_edit()
    {
        $id = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $isi    = $this->input->post('isi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('isi', 'Isi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/images/intro';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);
            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'isi' => $isi,
                        'gambar'      => $nama_gambar['file_name'],
                    );
                    $this->M_user_welcome->update_intro($data, $data_id);
                    $this->session->set_flashdata('msg', show_succ_msg('Intro Berhasil ditambahkan', '20px'));
                    redirect('user_welcome/intro');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Intro Gagal ditambahkan', '20px'));
                    redirect('user_welcome/intro');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'isi' => $isi,
                );
                $this->M_user_welcome->update_intro($data, $data_id);
                $this->session->set_flashdata('msg', show_succ_msg('Intro Berhasil ditambahkan', '20px'));
                redirect('user_welcome/intro');
            }
        }
    }

    public function intro_delete($id)
    {
        $where = array('id' => $id);
        $this->M_user_welcome->delete_intro($where, 'u_welcome_intro');
        $this->session->set_flashdata('msg', show_succ_msg('Intro Berhasil dihapus', '20px'));
        redirect('user_welcome/intro');
    }
    // Controller section Intro End


    // Controller section Gallery Start
    public function galeri()
    {
        $data['userdata']     = $this->userdata;

        $data['page']         = "galeri";
        $data['judul']         = "Galeri";
        $data['deskripsi']     = "Manage Galeri";

        $data['galeri'] = $this->M_user_welcome->get_galeri();
        $this->template->views('admin_view/user_welcome/galeri', $data);
    }

    public function galeri_add()
    {
        $judul        = $this->input->post('judul');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $config['upload_path']   = './assets/images/galeri';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul' => $judul,
                        'nama_gambar'      => $nama_gambar['file_name'],
                        'tanggal_upload' => date('Y-m-d H:i:s')
                    );
                    $this->M_user_welcome->insert_galeri($data);
                    // $this->session->set_flashdata('msg', '<div class="col-lg-14"><div class="alert alert-dismissable alert-danger">
                    // <button type="button" class="close" data-dismiss="alert">&times;</button>
                    // <strong>Upload Gagal!</strong>.</div></div>');
                    $this->session->set_flashdata('msg', show_succ_msg('Galeri Berhasil ditambahkan', '20px'));
                    redirect('user_welcome/galeri');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Galeri Gagal ditambahkan', '20px'));
                    redirect('user_welcome/galeri');
                }
            }
        }
    }

    public function galeri_edit()
    {
        $id = $this->input->post('id');

        $data_id = array('id' => $id);

        $config['upload_path']   = './assets/images/galeri';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size']      = '1048';
        $config['max_width']     = '1680';
        $config['max_height']    = '600';
        $config['file_name']     = $_FILES['nama_gambar']['name'];

        $this->upload->initialize($config);
        if (!empty($_FILES['nama_gambar']['name'])) {
            if ($this->upload->do_upload('nama_gambar')) {
                $nama_gambar = $this->upload->data();
                date_default_timezone_set("Asia/Jakarta");
                $data = array(
                    'nama_gambar'      => $nama_gambar['file_name'],
                    'tanggal_upload' => date('Y-m-d H:i:s')
                );
                $this->M_user_welcome->update_galeri($data, $data_id);
                $this->session->set_flashdata('msg', show_succ_msg('Galeri Berhasil ditambahkan', '20px'));
                redirect('user_welcome/galeri');
            } else {
                $this->session->set_flashdata('msg', show_err_msg('Galeri Gagal ditambahkan', '20px'));
                redirect('user_welcome/galeri');
            }
        }
    }

    public function galeri_delete($id)
    {
        $where = array('id' => $id);
        $this->M_user_welcome->delete_galeri($where, 'u_welcome_galeri');
        $this->session->set_flashdata('msg', show_succ_msg('Galeri Berhasil dihapus', '20px'));
        redirect('user_welcome/galeri');
    }
    // Controller section Gallery End
}
