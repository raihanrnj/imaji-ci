<?php
defined('BASEPATH') or exit('No direct script access allowed');
include_once(APPPATH . 'core/AUTH_Controller.php');

class User_profiles extends AUTH_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_user_profiles');
        $this->load->library('upload');
    }

    public function index()
    {
        redirect('user_profiles/pengantar');
    }

    public function pengantar()
    {
        $data['userdata']     = $this->userdata;

        $data['page']         = "pengantar";
        $data['judul']         = "Pengantar";
        $data['deskripsi']     = "Manage Pengantar";

        // $data = array('sliders' => $this->M_user_welcome->M_user_welcome());
        $data['pengantar'] = $this->M_user_profiles->get_pengantar();
        $this->template->views('admin_view/user_profiles/pengantar', $data);
    }

    public function pengantar_add()
    {
        $judul        = $this->input->post('judul');
        $isi_pengantar    = $this->input->post('isi_pengantar');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('isi_pengantar', 'Isi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                'judul'      => $judul,
                'isi' => $isi_pengantar,
                'diedit' => date('Y-m-d H:i:s'),
            );
            $this->M_user_profiles->insert($data);
            $this->session->set_flashdata('msg', show_succ_msg('Pengantar Berhasil ditambahkan', '20px'));
            redirect('user_profiles/pengantar');
        } else {
            $this->session->set_flashdata('msg', show_err_msg('Pengantar Gagal ditambahkan', '20px'));
            redirect('user_profiles/pengantar');
        }
    }

    public function pengantar_edit()
    {
        $id = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $isi_pengantar    = $this->input->post('isi_pengantar');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('isi_pengantar', 'Isi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);
            $data = array(
                'judul'      => $judul,
                'isi' => $isi_pengantar,
                'diedit' => date('Y-m-d H:i:s'),
            );
            $this->M_user_profiles->update($data, $data_id);
            $this->session->set_flashdata('msg', show_succ_msg('Slider Berhasil ditambahkan', '20px'));
            redirect('user_profiles/pengantar');
        } else {
            $this->session->set_flashdata('msg', show_err_msg('Slider Gagal ditambahkan', '20px'));
            redirect('user_profiles/pengantar');
        }
    }

    public function pengantar_delete($id)
    {
        $where = array('id' => $id);
        $this->M_user_profiles->delete_pengantar($where, 'u_profiles_pengantar');
        $this->session->set_flashdata('msg', show_succ_msg('Pengantar Berhasil dihapus', '20px'));
        redirect('user_profiles/pengantar');
    }

    public function akta_pendirian()
    {
        $data['userdata']     = $this->userdata;

        $data['page']         = "akta_pendirian";
        $data['judul']         = "Akta Pendirian";
        $data['deskripsi']     = "Manage Akta Pendirian";

        $data['akta_pendirian'] = $this->M_user_profiles->get_akta_pendirian();
        $this->template->views('admin_view/user_profiles/akta_pendirian', $data);
    }

    public function akta_pendirian_add()
    {
        $judul        = $this->input->post('judul');
        $isi_akta    = $this->input->post('isi_akta');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('isi_akta', 'Sub Judul', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/images/akta';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'isi' => $isi_akta,
                        'gambar'      => $nama_gambar['file_name'],
                    );
                    $this->M_user_profiles->insert_akta_pendirian($data);
                    $this->session->set_flashdata('msg', show_succ_msg('Akta Berhasil ditambahkan', '20px'));
                    redirect('user_profiles/akta_pendirian');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Akta Gagal ditambahkan', '20px'));
                    redirect('user_profiles/akta_pendirian');
                }
            }
        }
    }

    public function akta_pendirian_edit()
    {
        $id = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $isi_akta    = $this->input->post('isi_akta');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('isi_akta', 'Sub Judul', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/images/slider';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);
            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'isi' => $isi_akta,
                        'gambar'      => $nama_gambar['file_name'],
                    );
                    $this->M_user_profiles->update_akta_pendirian($data, $data_id);
                    $this->session->set_flashdata('msg', show_succ_msg('Akta Berhasil diupdate', '20px'));
                    redirect('user_profiles/akta_pendirian');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Akta Gagal diupdate', '20px'));
                    redirect('user_profiles/akta_pendirian');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'isi' => $isi_akta,
                );
                $this->M_user_profiles->update_akta_pendirian($data, $data_id);
                $this->session->set_flashdata('msg', show_succ_msg('Akta Berhasil diupdate', '20px'));
                redirect('user_profiles/akta_pendirian');
            }
        }
    }

    public function akta_pendirian_delete($id)
    {
        $where = array('id' => $id);
        $this->M_user_profiles->delete_akta_pendirian($where, 'u_profiles_akta_pendirian');
        $this->session->set_flashdata('msg', show_succ_msg('Akta Berhasil dihapus', '20px'));
        redirect('user_profiles/akta_pendirian');
    }

    public function lambang_dan_arti()
    {
        $data['userdata']     = $this->userdata;

        $data['page']         = "lambang_dan_arti";
        $data['judul']         = "Lambang dan Arti";
        $data['deskripsi']     = "Manage Lambang dan Arti";

        $data['lambang_dan_arti'] = $this->M_user_profiles->get_lambang_dan_arti();
        $this->template->views('admin_view/user_profiles/lambang_dan_arti', $data);
    }

    public function lambang_dan_arti_add()
    {
        $judul        = $this->input->post('judul');
        $isi_lambang_dan_arti    = $this->input->post('isi_lambang_dan_arti');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('isi_lambang_dan_arti', 'Sub Judul', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/images/lambang-dan-arti';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'isi' => $isi_lambang_dan_arti,
                        'gambar'      => $nama_gambar['file_name'],
                    );
                    $this->M_user_profiles->insert_lambang_dan_arti($data);
                    $this->session->set_flashdata('msg', show_succ_msg('Lambang dan Arti Berhasil ditambahkan', '20px'));
                    redirect('user_profiles/lambang_dan_arti');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Lambang dan Arti Gagal ditambahkan', '20px'));
                    redirect('user_profiles/lambang_dan_arti');
                }
            }
        }
    }

    public function lambang_dan_arti_edit()
    {
        $id = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $isi_lambang_dan_arti    = $this->input->post('isi_lambang_dan_arti');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('isi_lambang_dan_arti', 'Sub Judul', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/images/lambang-dan-arti';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);
            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'isi' => $isi_lambang_dan_arti,
                        'gambar'      => $nama_gambar['file_name'],
                    );
                    $this->M_user_profiles->update_lambang_dan_arti($data, $data_id);
                    $this->session->set_flashdata('msg', show_succ_msg('Lambang dan Arti Berhasil diupdate', '20px'));
                    redirect('user_profiles/lambang_dan_arti');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Lambang dan Arti Gagal diupdate', '20px'));
                    redirect('user_profiles/lambang_dan_arti');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'isi' => $isi_lambang_dan_arti,
                );
                $this->M_user_profiles->update_lambang_dan_arti($data, $data_id);
                $this->session->set_flashdata('msg', show_succ_msg('Lambang dan Arti Berhasil diupdate', '20px'));
                redirect('user_profiles/lambang_dan_arti');
            }
        }
    }

    public function lambang_dan_arti_delete($id)
    {
        $where = array('id' => $id);
        $this->M_user_profiles->delete_lambang_dan_arti($where, 'u_profiles_lambang_dan_arti');
        $this->session->set_flashdata('msg', show_succ_msg('Lambang dan Arti Berhasil dihapus', '20px'));
        redirect('user_profiles/lambang_dan_arti');
    }

    public function visi_misi()
    {
        $data['userdata']     = $this->userdata;

        $data['page']         = "visi_misi";
        $data['judul']         = "Visi dan Misi";
        $data['deskripsi']     = "Manage Visi dan Misi";

        $data['visi_misi'] = $this->M_user_profiles->get_visi_misi();
        $this->template->views('admin_view/user_profiles/visi_misi', $data);
    }

    public function visi_misi_add()
    {
        $visi        = $this->input->post('visi');
        $misi    = $this->input->post('misi');

        $this->form_validation->set_rules('visi', 'Visi', 'trim|required');
        $this->form_validation->set_rules('misi', 'Misi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                'visi'      => $visi,
                'misi' => $misi,
            );
            $this->M_user_profiles->insert_visi_misi($data);
            $this->session->set_flashdata('msg', show_succ_msg('Visi dan Misi Berhasil ditambahkan', '20px'));
            redirect('user_profiles/visi_misi');
        } else {
            $this->session->set_flashdata('msg', show_err_msg('Visi dan Misi Gagal ditambahkan', '20px'));
            redirect('user_profiles/visi_misi');
        }
    }


    public function visi_misi_edit()
    {
        $id = $this->input->post('id');
        $visi        = $this->input->post('visi');
        $misi    = $this->input->post('misi');

        $this->form_validation->set_rules('visi', 'Visi', 'trim|required');
        $this->form_validation->set_rules('misi', 'Misi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);
            $data = array(
                'visi'      => $visi,
                'misi' => $misi,
            );
            $this->M_user_profiles->update_visi_misi($data, $data_id);
            $this->session->set_flashdata('msg', show_succ_msg('Visi dan Misi Berhasil diupdate', '20px'));
            redirect('user_profiles/visi_misi');
        } else {
            $this->session->set_flashdata('msg', show_err_msg('Visi dan Misi Gagal diupdate', '20px'));
            redirect('user_profiles/visi_misi');
        }
    }

    public function visi_misi_delete($id)
    {
        $where = array('id' => $id);
        $this->M_user_profiles->delete_visi_misi($where, 'u_profiles_visi_misi');
        $this->session->set_flashdata('msg', show_succ_msg('Visi dan Misi Berhasil dihapus', '20px'));
        redirect('user_profiles/visi_misi');
    }

    //kode etik start
    public function kode_etik()
    {
        $data['userdata']     = $this->userdata;

        $data['page']         = "kode_etik";
        $data['judul']         = "Kode Etik";
        $data['deskripsi']     = "Manage Kode Etik";

        $data['kode_etik'] = $this->M_user_profiles->get_kode_etik();
        $this->template->views('admin_view/user_profiles/kode_etik', $data);
    }

    public function kode_etik_add()
    {
        $judul        = $this->input->post('judul');
        $isi    = $this->input->post('isi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('isi', 'Isi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                'judul'      => $judul,
                'isi' => $isi,
            );
            $this->M_user_profiles->insert_kode_etik($data);
            $this->session->set_flashdata('msg', show_succ_msg('Kode Etik Berhasil ditambahkan', '20px'));
            redirect('user_profiles/kode_etik');
        } else {
            $this->session->set_flashdata('msg', show_err_msg('Kode Etik Gagal ditambahkan', '20px'));
            redirect('user_profiles/kode_etik');
        }
    }


    public function kode_etik_edit()
    {
        $id = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $isi    = $this->input->post('isi');

        $this->form_validation->set_rules('judul', 'judul', 'trim|required');
        $this->form_validation->set_rules('isi', 'isi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);
            $data = array(
                'judul'      => $judul,
                'isi' => $isi,
            );
            $this->M_user_profiles->update_kode_etik($data, $data_id);
            $this->session->set_flashdata('msg', show_succ_msg('Kode Etik Berhasil diupdate', '20px'));
            redirect('user_profiles/kode_etik');
        } else {
            $this->session->set_flashdata('msg', show_err_msg('Kode Etik Gagal diupdate', '20px'));
            redirect('user_profiles/kode_etik');
        }
    }

    public function kode_etik_delete($id)
    {
        $where = array('id' => $id);
        $this->M_user_profiles->delete_kode_etik($where, 'u_profiles_kode_etik');
        $this->session->set_flashdata('msg', show_succ_msg('Kode Etik Berhasil dihapus', '20px'));
        redirect('user_profiles/kode_etik');
    }
}
