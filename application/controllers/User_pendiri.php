<?php
defined('BASEPATH') or exit('No direct script access allowed');
include_once(APPPATH . 'core/AUTH_Controller.php');

class User_pendiri extends AUTH_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_user_pendiri');
        $this->load->library('upload');
    }

    function index()
    {
        $data['userdata']     = $this->userdata;

        $data['page']         = "pendiri";
        $data['judul']         = "Pendiri";
        $data['deskripsi']     = "Manage Pendiri";

        $data['pendiri'] = $this->M_user_pendiri->get_pendiri_all();
        $data['provinsi'] = $this->M_user_pendiri->get_provinsi();
        $this->template->views('admin_view/user_pendiri/home', $data);
    }

    function post_pendiri()
    {
        $nama        = $this->input->post('nama');
        $alamat    = $this->input->post('alamat');
        $afiliasi    = $this->input->post('afiliasi');

        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('afiliasi', 'afiliasi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/images/pendiri';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'nama'      => $nama,
                        'alamat' => $alamat,
                        'foto'      => $nama_gambar['file_name'],
                        'afiliasi'      => $afiliasi,
                    );
                    $this->M_user_pendiri->insert_pendiri($data);
                    $this->session->set_flashdata('msg', show_succ_msg('Data Pendiri Berhasil ditambahkan', '20px'));
                    redirect('user_pendiri');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Data Pendiri Gagal ditambahkan', '20px'));
                    redirect('user_pendiri');
                }
            }
        }
    }

    public function pendiri_edit()
    {
        $id = $this->input->post('id');
        $nama        = $this->input->post('nama');
        $alamat    = $this->input->post('alamat');
        $afiliasi    = $this->input->post('afiliasi');

        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('afiliasi', 'afiliasi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $id = array('id' => $id);

            $config['upload_path']   = './assets/images/pendiri';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['nama_gambar']['name'];

            $this->upload->initialize($config);
            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    $nama_gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'nama'      => $nama,
                        'alamat' => $alamat,
                        'foto'      => $nama_gambar['file_name'],
                        'afiliasi'      => $afiliasi,
                    );
                    $this->M_user_pendiri->update_pendiri($data, $id);
                    $this->session->set_flashdata('msg', show_succ_msg('Data Pendiri Berhasil diupdate', '20px'));
                    redirect('user_pendiri');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Data Pendiri Gagal diupdate', '20px'));
                    redirect('user_pendiri');
                }
            } else {
                $data = array(
                    'nama'      => $nama,
                    'alamat' => $alamat,
                    'afiliasi' => $afiliasi,
                );
                $this->M_user_pendiri->update_pendiri($data, $id);
                $this->session->set_flashdata('msg', show_succ_msg('Data Pendiri Berhasil diupdate', '20px'));
                redirect('user_pendiri');
            }
        }
    }

    public function pendiri_delete($id)
    {
        $where = array('id' => $id);
        $this->M_user_pendiri->delete_pendiri($where, 'u_pendiri');
        $this->session->set_flashdata('msg', show_succ_msg('Data Pendiri Berhasil dihapus', '20px'));
        redirect('user_pendiri');
    }

    function description()
    {
        $data['userdata']     = $this->userdata;

        $data['page']         = "desc_pendiri";
        $data['judul']         = "Deskripsi Pendiri";
        $data['deskripsi']     = "Deskripsi Pendiri";

        $data['desc_pendiri'] = $this->M_user_pendiri->get_desc_pendiri_all();
        $this->template->views('admin_view/user_pendiri/sec_title', $data);
    }

    function description_add()
    {
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                'judul'      => $judul,
                'deskripsi' => $deskripsi,
            );
            $this->M_user_pendiri->insert_deskripsi($data);
            $this->session->set_flashdata('msg', show_succ_msg('Deskripsi Berhasil ditambahkan', '20px'));
            redirect('user_pendiri/description');
        } else {
            $this->session->set_flashdata('msg', show_err_msg('Deskripsi Gagal ditambahkan', '20px'));
            redirect('user_pendiri/description');
        }
    }

    function description_edit()
    {
        $id = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $id = array('id' => $id);
            $data = array(
                'judul'      => $judul,
                'deskripsi' => $deskripsi,
            );
            $this->M_user_pendiri->update_deskripsi($data, $id);
            $this->session->set_flashdata('msg', show_succ_msg('Deskripsi Berhasil diupdate', '20px'));
            redirect('user_pendiri/description');
        } else {
            $this->session->set_flashdata('msg', show_err_msg('Deskripsi Gagal diupdate', '20px'));
            redirect('user_pendiri/description');
        }
    }

    public function description_delete($id)
    {
        $where = array('id' => $id);
        $this->M_user_pendiri->delete_deskripsi($where, 'pendiri_desc');
        $this->session->set_flashdata('msg', show_succ_msg('Deskripsi Berhasil dihapus', '20px'));
        redirect('user_pendiri/description');
    }
}
