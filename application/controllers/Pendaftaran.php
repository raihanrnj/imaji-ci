<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pendaftaran extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_wilayah');
        $this->load->model('M_pendaftaran');
        $this->load->library('form_validation');
        $this->load->library('upload');
    }

    function index()
    {
        $data['title'] = "Dynamic Dependent";
        $data['provinsi'] = $this->M_wilayah->get_provinsi();
        $this->load->view('dynamic_dependent', $data);
    }

    //request data kabupaten berdasarkan id provinsi yang dipilih
    function get_kabupaten()
    {
        if ($this->input->post('provinsi_id')) {
            echo $this->M_wilayah->get_kabupaten($this->input->post('provinsi_id'));
        }
    }

    //request data kecamatan berdasarkan id kabupaten yang dipilih
    function get_kecamatan()
    {
        if ($this->input->post('kabupaten_id')) {
            echo $this->M_wilayah->get_kecamatan($this->input->post('kabupaten_id'));
        }
    }

    //request data desa berdasarkan id kecamatan yang dipilih
    function get_desa()
    {
        if ($this->input->post('kecamatan_id')) {
            echo $this->M_wilayah->get_desa($this->input->post('kecamatan_id'));
        }
    }

    public function daftar()
    {
        $nama_lengkap = $this->input->post('nama_lengkap');
        $gelar_depan = $this->input->post('gelar_depan');
        $gelar_belakang = $this->input->post('gelar_belakang');
        $pas_foto = $_FILES['pas_foto']['name'];
        $tempat_lahir = $this->input->post('tempat_lahir');
        $tanggal_lahir = $this->input->post('tanggal_lahir');
        $no_nik = $this->input->post('no_nik');
        $kartu_identitas = $_FILES['kartu_identitas']['name'];

        $alamat = $this->input->post('alamat');
        $provinsi = $this->input->post('provinsi');
        $kota = $this->input->post('kota');
        $kode_pos = $this->input->post('kode_pos');
        $no_hp = $this->input->post('no_hp');

        $no_telp_kantor = $this->input->post('no_telp_kantor');
        $email_pribadi = $this->input->post('email_pribadi');
        $email_bisnis = $this->input->post('email_bisnis');
        $nama_institusi = $this->input->post('nama_institusi');
        $alamat_institusi = $this->input->post('alamat_institusi');

        $provinsi_institusi = $this->input->post('provinsi_institusi');
        $kota_institusi = $this->input->post('kota_institusi');
        $kode_pos_institusi = $this->input->post('kode_pos_institusi');
        $program_studi = $this->input->post('program_studi');
        $bidang_keahlian = $this->input->post('bidang_keahlian');
        $scopus_id = $this->input->post('scopus_id');

        $orcid_id = $this->input->post('orcid_id');
        $google_s_id = $this->input->post('google_s_id');
        $sinta_id = $this->input->post('sinta_id');
        $publon_id = $this->input->post('publon_id');
        $profesi = $this->input->post('profesi');

        $nama_jurnal = $this->input->post('nama_jurnal');
        $pic = $this->input->post('pic');
        $issn = $this->input->post('issn');
        $url_jurnal = $this->input->post('url_jurnal');
        $nim = $this->input->post('nim');

        $foto_ktm = $_FILES['foto_ktm']['name'];
        $administrasi_profesi = $this->input->post('administrasi_profesi');

        if ($pas_foto !== "") {

            $config['file_name'] = $_FILES['pas_foto']['name'];
            $config['upload_path'] = './assets/images/pendaftaran';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size'] = '0';
            $config['log_threshold'] = 1;
            $this->upload->initialize($config);
            $this->upload->do_upload('pas_foto');
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];
            $pas_foto = $file_name;
        };
        if ($kartu_identitas !== "") {

            $config['file_name'] = $_FILES['kartu_identitas']['name'];
            $config['upload_path'] = './assets/images/pendaftaran';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size'] = '0';
            $this->upload->initialize($config);
            $this->upload->do_upload('kartu_identitas');
            $upload_data = $this->upload->data();
            $file_name1 = $upload_data['file_name'];
            $kartu_identitas = $file_name1;
        };
        if ($foto_ktm !== "") {
            $config['file_name'] = $_FILES['foto_ktm']['name'];
            $config['upload_path'] = './assets/images/pendaftaran';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size'] = '0';
            $this->upload->initialize($config);
            $this->upload->do_upload('foto_ktm');
            $upload_data = $this->upload->data();
            $file_name2 = $upload_data['file_name'];
            $foto_ktm = $file_name2;
        }

        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required');
        $this->form_validation->set_rules('pas_foto', '', 'xss_clean');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'trim|required');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'trim|required');
        $this->form_validation->set_rules('no_nik', 'NIK', 'trim|required');
        $this->form_validation->set_rules('kartu_identitas', '', 'xss_clean');
        $this->form_validation->set_rules('alamat', 'Alamat');
        $this->form_validation->set_rules('provinsi', 'Provinsi');
        $this->form_validation->set_rules('kota', 'Kota');
        $this->form_validation->set_rules('kode_pos', 'Kode Pos');
        $this->form_validation->set_rules('no_hp', 'No Handphone');
        $this->form_validation->set_rules('no_telp_kantor', 'No telpone Kantor');
        $this->form_validation->set_rules('email_pribadi', 'Email Pribadi');
        $this->form_validation->set_rules('nama_institusi', 'Nama Institusi');
        $this->form_validation->set_rules('alamat_institusi', 'Alamat Institusi');
        $this->form_validation->set_rules('provinsi_institusi', 'Provinsi Institusi');
        $this->form_validation->set_rules('kota_institusi', 'Kota Institusi');
        $this->form_validation->set_rules('kode_pos_institusi', 'Kode Post Intotusi');
        $this->form_validation->set_rules('program_studi', 'Program Studi');
        $this->form_validation->set_rules('profesi', 'Profesi');
        $this->form_validation->set_rules('nama_jurnal', 'Nama Jurnal');
        $this->form_validation->set_rules('administrasi_profesi', 'Administrasi Profesi');

        if ($administrasi_profesi == '150000') {
            $durasi_keanggotaan = 12;
        } elseif ($administrasi_profesi == '250000') {
            $durasi_keanggotaan = 24;
        } elseif ($administrasi_profesi == '350000') {
            $durasi_keanggotaan = 36;
        } elseif ($administrasi_profesi == '50000') {
            $durasi_keanggotaan = 12;
        };

        $taggal_sekarang = date("Y-m-d");
        $tanggal_expired = date("Y-m-d", strtotime($taggal_sekarang . '+' . $durasi_keanggotaan . 'Months'));

        // $urut = date("Ymd");
        if ($this->form_validation->run() !== false) {
            $data = array(
                // 'id' => '',
                'kode_anggota' => $this->M_pendaftaran->get_maxKodeAnggota(),
                'password' => md5(date("Ymd", strtotime($tanggal_lahir))),
                'status' => 'non aktif',
                'tanggal_join' => date('Y-m-d'),
                'tanggal_expired' => $tanggal_expired,
                'level' => 'anggota',
                'nama_lengkap' => $nama_lengkap,
                'gelar_depan' => $gelar_depan,
                'gelar_belakang' => $gelar_belakang,
                'pas_foto' => $pas_foto,
                'tempat_lahir' => $tempat_lahir,
                'tanggal_lahir' => $tanggal_lahir,
                'no_nik' => $no_nik,
                'kartu_identitas' => $kartu_identitas,
                'alamat' => $alamat,
                'provinsi' => $provinsi,
                'kota' => $kota,
                'kode_pos' => $kode_pos,
                'no_hp' => $no_hp,
                'no_telp_kantor' => $no_telp_kantor,
                'email_pribadi' => $email_pribadi,
                'email_bisnis' => $email_bisnis,
                'nama_institusi' => $nama_institusi,
                'alamat_institusi' => $alamat_institusi,
                'provinsi_institusi' => $provinsi_institusi,
                'kota_institusi' => $kota_institusi,
                'kode_pos_institusi' => $kode_pos_institusi,
                'program_studi' => $program_studi,
                'bidang_keahlian' => $bidang_keahlian,
                'scopus_id' => $scopus_id,
                'orcid_id' => $orcid_id,
                'google_s_id' => $google_s_id,
                'sinta_id' => $sinta_id,
                'publon_id' => $publon_id,
                'profesi' => $profesi,
                'nama_jurnal' => $nama_jurnal,
                'pic' => $pic,
                'issn' => $issn,
                'url_jurnal' => $url_jurnal,
                'nim' => $nim,
                'foto_ktm' => $foto_ktm,
                'administrasi_profesi' => $administrasi_profesi,
            );
            $harga['harga'] = $administrasi_profesi;
            $this->M_pendaftaran->insert_pendaftaran($data);
            // $this->M_pendaftaran->insert_admin($data);


            $this->session->set_flashdata('msg', $harga['harga']);
            redirect('page/view/berhasil_daftar');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger f-bold" role="alert">
            <i class="tf-ion-android-danger"></i>
            Pendaftaran Gagal ! silahkan ulangi lagi
            </div>');
            redirect('page/view/pendaftaran');
        }
    }
}
