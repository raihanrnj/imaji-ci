<?php
defined('BASEPATH') or exit('No direct script access allowed');
include_once(APPPATH . 'core/AUTH_Controller.php');

class User_contact extends AUTH_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_user_contact');
    }

    public function index()
    {
        redirect('user_contact/contact');
    }

    public function contact()
    {
        $data['userdata']     = $this->userdata;

        $data['page']         = "contact";
        $data['judul']         = "Contact";
        $data['deskripsi']     = "Manage Contact";

        $data['contact'] = $this->M_user_contact->get_contact();
        $this->template->views('admin_view/user_contact/home', $data);
    }

    public function contact_add()
    {
        $alamat        = $this->input->post('alamat');
        $no_hp    = $this->input->post('no_hp');
        $email    = $this->input->post('email');
        $username_fb    = $this->input->post('username_fb');
        $username_tw    = $this->input->post('username_tw');
        $username_ig    = $this->input->post('username_ig');
        $username_lk    = $this->input->post('username_lk');

        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('no_hp', 'No HP', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('username_fb', 'Facebook', 'trim|required');
        $this->form_validation->set_rules('username_tw', 'Twitter', 'trim|required');
        $this->form_validation->set_rules('username_ig', 'Instagram', 'trim|required');
        $this->form_validation->set_rules('username_lk', 'Linkedin', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                'alamat'      => $alamat,
                'no_hp' => $no_hp,
                'email' => $email,
                'username_fb' => $username_fb,
                'username_tw' => $username_tw,
                'username_ig' => $username_ig,
                'username_lk' => $username_lk,
            );
            $this->M_user_contact->insert($data);
            $this->session->set_flashdata('msg', show_succ_msg('Kontak Berhasil ditambahkan', '20px'));
            redirect('user_contact/contact');
        } else {
            $this->session->set_flashdata('msg', show_err_msg('Kontak Gagal ditambahkan', '20px'));
            redirect('user_contact/contact');
        }
    }

    public function contact_edit()
    {
        $id = $this->input->post('id');
        $alamat        = $this->input->post('alamat');
        $no_hp    = $this->input->post('no_hp');
        $email    = $this->input->post('email');
        $username_fb    = $this->input->post('username_fb');
        $username_tw    = $this->input->post('username_tw');
        $username_ig    = $this->input->post('username_ig');
        $username_lk    = $this->input->post('username_lk');

        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('no_hp', 'No HP', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('username_fb', 'Facebook', 'trim|required');
        $this->form_validation->set_rules('username_tw', 'Twitter', 'trim|required');
        $this->form_validation->set_rules('username_ig', 'Instagram', 'trim|required');
        $this->form_validation->set_rules('username_lk', 'Linkedin', 'trim|required');


        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);
            $data = array(
                'alamat'      => $alamat,
                'no_hp' => $no_hp,
                'email' => $email,
                'username_fb' => $username_fb,
                'username_tw' => $username_tw,
                'username_ig' => $username_ig,
                'username_lk' => $username_lk,
            );
            $this->M_user_contact->update($data, $data_id);
            $this->session->set_flashdata('msg', show_succ_msg('Kontak Berhasil ditambahkan', '20px'));
            redirect('user_contact/contact');
        } else {
            $this->session->set_flashdata('msg', show_err_msg('Kontak Gagal ditambahkan', '20px'));
            redirect('user_contact/contact');
        }
    }

    public function contact_delete($id)
    {
        $where = array('id' => $id);
        $this->M_user_contact->delete_contact($where, 'u_contact');
        $this->session->set_flashdata('msg', show_succ_msg('Contact Berhasil dihapus', '20px'));
        redirect('user_contact/contact');
    }
}
