<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH.'core/AUTH_Controller.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

class Validasi_anggota extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		require APPPATH.'libraries/phpmailer/src/Exception.php';
        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH.'libraries/phpmailer/src/SMTP.php';

		$this->load->model('M_validasianggota');
		$this->load->model('M_posisi');
		$this->load->model('M_kota');
	}

	public function index() {
		$data['userdata'] = $this->userdata;
		$data['dataAnggota'] = $this->M_validasianggota->select_all();
		$data['dataPosisi'] = $this->M_posisi->select_all();
		$data['dataKota'] = $this->M_kota->select_all();

		$data['page'] = "validasi_anggota";
		$data['judul'] = "Validasi Data Anggota";
		$data['deskripsi'] = "Manage Data Anggota";

		$this->template->views('admin_view/validasi_anggota/home', $data);
	}

	public function tampil() {
		
		$data['dataValidasianggota'] = $this->M_validasianggota->select_all();
		$this->load->view('admin_view/validasi_anggota/list_data', $data);
	}

	public function update() {
		$kode_anggota = trim($_POST['id']);

		$data['dataAnggota'] = $this->M_validasianggota->select_by_id($kode_anggota);
		$data['userdata'] = $this->userdata;

		echo show_my_modal('admin_view/modals/modal_update_validasianggota', 'update-validasianggota', $data);
	}

	public function prosesUpdate() {

		$data = $this->input->post();
		$kode_anggota = $data['kode_anggota'];

		$data_anggota = $this->M_validasianggota->select_by_id($kode_anggota);
		// var_dump($data_anggota);
		// die();
		$email_pribadi = $data_anggota->email_pribadi;
		$password = date("Ymd",strtotime($data_anggota->tanggal_lahir));
	
	
			$result = $this->M_validasianggota->updateStatus($data);

			if ($result > 0) {
				if ($data['status'] == 'aktif'){
			
						//START EMAIL SMTP
						$mail = new PHPMailer(true);
						// $mail->SMTPDebug = SMTP::DEBUG_SERVER;
						$mail->isSMTP();
						$mail->SMTPAuth   = true;
					
						$mail->SMTPAuth   = true;                  // enable SMTP authentication
						$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
						$mail->Host       = "masyarakatjurnal.or.id";      // sets YAHOO as the SMTP server
						$mail->Port       = 465;  
						$mail->protocol = "mail";
						$mail->smtp_port = 587; 
						$mail->Username   = "admin@masyarakatjurnal.or.id";  // GMAIL username
						$mail->Password   = "imaji2021";            // GMAIL password

						$mail->SetFrom('admin@masyarakatjurnal.or.id', 'Pembayaran tervalidasi');
						$mail->addReplyTo('admin@masyarakatjurnal.or.id', '' );
						$mail->addAddress( $email_pribadi, '' );

						$mail->isHTML(true);
						$mail->Subject = "Email Konfirmasi IMAJI";
						$mailContent = "<html>
										<center>
										<head>
										<img src='http://masyarakatjurnal.or.id/assets/images/logo.png' style='height:66px; margin-bottom:10px;'>
										<title>Pembayaran telah divalidasi</title>
										</head>
										<body>
										<h2>Selamat bergabung dengan IMAJI.</h2>
										<p>Sekarang anda dapat login dengan akun:</p>
										<p>Email: " . $email_pribadi . "</p>
										<p>Password: " . $password . "</p>
										<p><a href='http://masyarakatjurnal.or.id/auth' > Klik untuk login ke Web IMAJI. </a></p>
										</body>
										</center>
										</html>";
						$mail->Body = $mailContent;
						$mail->send();

				}


				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Anggota Berhasil diupdate', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Anggota Gagal diupdate', '20px');
			}
		
		echo json_encode($out);
	}

	public function delete() {
		$id = $_POST['id'];
		$result = $this->M_validasianggota->delete($id);

		if ($result > 0) {
			echo show_succ_msg('Data Anggota Berhasil dihapus', '20px');
		} else {
			echo show_err_msg('Data Anggota Gagal dihapus', '20px');
		}
	}
	
}
