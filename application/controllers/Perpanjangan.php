<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perpanjangan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_perpanjangan');
        $this->load->library('form_validation');
    }

    function index()
    {
        $kode_anggota = $this->input->post('kode_anggota');

        $this->form_validation->set_rules('kode_anggota', 'Kode Anggota', 'trim|required');

        $where = array(
            'kode_anggota' => $kode_anggota,
        );

        $data = $this->M_perpanjangan->cek_data($where, 'anggota');
        $d = $this->M_perpanjangan->cek_data($where, 'anggota')->row();
        $cek = $data->num_rows();

        if ($cek > 0) {
            $expired = $d->tanggal_expired;

            $masa_tenggang = 2;
            $sekarang = date("Y-m-d");
            $jadi_masa_tenggang = date("Y-m-d", strtotime($expired . '+' . $masa_tenggang . "Months"));

            if ($sekarang >= $jadi_masa_tenggang) {
                $this->session->set_flashdata('msg', '<div class="col-lg-14"><div class="alert alert-dismissable alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Akun anda sudah di non aktifkan silahkan registrasi ulang !</strong></div></div>');
                redirect('page/view/pendaftaran');
            } else {
                $this->session->set_flashdata('msg', '    <div class="container">
                <div class="alert alert-success" style="line-height: 150%;" role="alert">
                    <i class="tf-ion-android-success"></i>
                        <div class="row ml-0">
                            Kode Anggota : <div class="f-bold pl-1 mb-3">' . $kode_anggota . '</div>
                        </div>
                        <div class="f-bold">Terima kasih akun anda masih dapat diperpanjang</div>
                            <div>Untuk memperpanjang akun anda, silahkan transfer uang keanggotaan ke rekening berikut :</div>
                                <div class="my-3">
                                    <div class="f-bold">No rek : 1232323232</div>
                                    <div class="f-bold"> a/n : Raihan</div>
                                    <div class="f-bold"> Nominal : 150.000</div>
                                </div>
                            <div> Kemudian kirim bukti transfer ke email raihanrnj@gmail.com dengan SUBJECT</div>
                        <div class="f-bold">“KONFIRMASI PEMBAYARAN PERPANJANGAN KEANGGOTAAN IMAJI - [NIK KTP anda]”</div>
                    </div>
                 </div>');
                redirect('page/view/perpanjangan');
            }
        } else {
            $this->session->set_flashdata('msg', '<div class="col-lg-14"><div class="alert alert-dismissable alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Kode anggota tidak terdaftar !</strong></div></div>');
            redirect('page/view/perpanjangan');
        }
    }
}
