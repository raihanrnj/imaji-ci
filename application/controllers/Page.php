<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->model('M_wilayah');
        $this->load->model('M_user_profiles');
        $this->load->model('M_berita');
        $this->load->model('M_user_contact');
        $this->load->model('M_user_keanggotaan');
        $this->load->model('M_jurnal');
        $this->load->model('M_user_pendiri');
        $this->load->model('M_user_pengurus');
    }

    public function index()
    {
        redirect('page/view/home');
    }

    public function view($page = 'home')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            show_404();
        }

        $data['title'] = ucfirst($page);
        $data['page'] = $page;
        $data['provinsi'] = $this->M_wilayah->get_provinsi();

        $data['pengantar'] = $this->M_user_profiles->get_pengantar();
        $data['akta_pendirian'] = $this->M_user_profiles->get_akta_pendirian();
        $data['lambang_dan_arti'] = $this->M_user_profiles->get_lambang_dan_arti();
        $data['visi_misi'] = $this->M_user_profiles->get_visi_misi();
        $data['kode_etik'] = $this->M_user_profiles->get_kode_etik();
        $data['contact'] = $this->M_user_contact->get_contact();
        $data['keanggotaan'] = $this->M_user_keanggotaan->get_keanggotaan();
        $data['info_keanggotaan'] = $this->M_user_keanggotaan->get_info_keanggotaan();
        $data['pendiri'] = $this->M_user_pendiri->get_pendiri_all();
        $data['desc_pendiri'] = $this->M_user_pendiri->get_desc_pendiri_all();
        $data['desc_pengurus'] = $this->M_user_pengurus->get_desc_pengurus_all();


        $perpage = 6;
        $offset = $this->uri->segment(4);
        switch ($page) {
            case 'berita':
                $data['berita'] = $this->M_berita->getDataPagination($perpage, $offset)->result();
                $this->get_berita_pag();
                break;
            case 'berita_old':
                $data['berita'] = $this->M_berita->getDataPagination_old($perpage, $offset)->result();
                $this->get_berita_pag();
                break;
            case 'jurnal':
                $data['jurnal'] = $this->M_jurnal->getDataPagination($perpage, $offset)->result();
                $this->get_jurnal_pag();
                break;
            case 'jurnal_old':
                $data['jurnal'] = $this->M_jurnal->getDataPagination_old($perpage, $offset)->result();
                $this->get_jurnal_pag();
                break;
        }

        $this->load->view('templates/navbar', $data);
        if ($page == 'home') {
            $this->load->view('templates/slider', $data);
        } else if ($page == 'berhasil_daftar') {
        } else {
            $this->load->view('templates/breadcrumb', $data);
        }
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);
    }

    function views()
    {
        //SET SESSION FOR CONTACT
        $data['contact'] = $this->M_user_contact->get_contact()[0];
		$session = [
			'username_ig' => $data["contact"]["username_ig"],
			'username_tw' => $data["contact"]["username_tw"],
			'username_lk' => $data["contact"]["username_lk"],
			'username_fb' => $data["contact"]["username_fb"],
		];
		$this->session->set_userdata($session);

        $pages['page'] = "detail berita";
        $kode = $this->uri->segment(3);
        $data['berita'] = $this->M_berita->get_berita_by_kode($kode);
        $data['berita_terbaru'] = $this->M_berita->get_berita_terbaru();
        $this->load->view('templates/navbar', $pages);
        $this->load->view('pages/berita_detail', $data);
        $this->load->view('templates/footer');
    }
    public function get_data_chart()
    {
        $data['diagram'] = $this->M_user_pengurus->get_data_chart();
        echo json_encode($data);
    }
}
