<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_user_welcome');
		$this->load->model('M_user_contact');
		$this->load->model('M_berita');
	}

	public function index()
	{
		$data['page'] = "Welcome";
		$data['slider'] = $this->M_user_welcome->get_slider();
		$data['intro'] = $this->M_user_welcome->get_intro();
		$data['galeri'] = $this->M_user_welcome->get_galeri();
		$data['berita'] = $this->M_berita->get_berita();

		$data['contact'] = $this->M_user_contact->get_contact()[0];
		$session = [
			'username_ig' => $data["contact"]["username_ig"],
			'username_tw' => $data["contact"]["username_tw"],
			'username_lk' => $data["contact"]["username_lk"],
			'username_fb' => $data["contact"]["username_fb"],
		];
		$this->session->set_userdata($session);

		$this->load->view('templates/navbar', $data);
		$this->load->view('templates/slider');
		$this->load->view('pages/home');
		$this->load->view('templates/footer');
	}
}
