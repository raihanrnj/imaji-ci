<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH.'core/AUTH_Controller.php');

class Anggota extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_anggota');
		$this->load->model('M_posisi');
		$this->load->model('M_kota');
	}

	public function index() {
		$data['userdata'] = $this->userdata;
		$data['dataAnggota'] = $this->M_anggota->select_all();
		$data['dataPosisi'] = $this->M_posisi->select_all();
		$data['dataKota'] = $this->M_kota->select_all();

		$data['page'] = "anggota";
		$data['judul'] = "Data Anggota";
		$data['deskripsi'] = "Manage Data Anggota";

		$data['modal_tambah_anggota'] = show_my_modal('admin_view/modals/modal_tambah_anggota', 'tambah-anggota', $data);

		$this->template->views('admin_view/anggota/home', $data);
	}

	public function tampil() {
		$data['dataAnggota'] = $this->M_anggota->select_all();
		$this->load->view('admin_view/anggota/list_data', $data);
	}

	public function prosesTambah() {
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('kota', 'Kota', 'trim|required');
		$this->form_validation->set_rules('jk', 'Jenis Kelamin', 'trim|required');
		$this->form_validation->set_rules('posisi', 'Posisi', 'trim|required');

		$data = $this->input->post();
		if ($this->form_validation->run() == TRUE) {
			$result = $this->M_anggota->insert($data);
			// $result2 = $this->M_anggota->insert_admin($data);

			if ($result > 0 && $result2 > 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Anggota Berhasil ditambahkan', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_err_msg('Data Anggota Gagal ditambahkan', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function update() {
		$kode_anggota = trim($_POST['id']);

		$data['dataAnggota'] = $this->M_anggota->select_by_id($kode_anggota);
		$data['userdata'] = $this->userdata;

		echo show_my_modal('admin_view/modals/modal_update_anggota', 'update-anggota', $data);
	}

	public function prosesUpdate() {

		$data = $this->input->post();
	
	
			$result = $this->M_anggota->updateStatus($data);

			if ($result > 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Anggota Berhasil diupdate', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Anggota Gagal diupdate', '20px');
			}
		
		echo json_encode($out);
	}

	public function delete() {
		$id = $_POST['id'];
		$result = $this->M_anggota->delete($id);

		if ($result > 0) {
			echo show_succ_msg('Data Anggota Berhasil dihapus', '20px');
		} else {
			echo show_err_msg('Data Anggota Gagal dihapus', '20px');
		}
	}

	
}

