<?php
defined('BASEPATH') or exit('No direct script access allowed');
include_once(APPPATH . 'core/AUTH_Controller.php');

class User_pengurus extends AUTH_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_user_pengurus');
        $this->load->library('upload');
    }

    function index()
    {
        $data['userdata']     = $this->userdata;

        $data['page']         = "pengurus";
        $data['judul']         = "Pengurus";
        $data['deskripsi']     = "Manage Pengurus";

        $data['pengurus'] = $this->M_user_pengurus->get_pengurus_all();
        $data['provinsi'] = $this->M_user_pengurus->get_provinsi();
        $this->template->views('admin_view/user_pengurus/home', $data);
    }

    function post_pengurus()
    {
        $nama        = $this->input->post('nama');
        $alamat    = $this->input->post('alamat');
        $jabatan    = $this->input->post('jabatan');
        $afiliasi    = $this->input->post('afiliasi');

        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('jabatan', 'jabatan', 'trim|required');
        $this->form_validation->set_rules('afiliasi', 'afiliasi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $new_name = $_FILES['nama_gambar']['name'];
            $new_names =  base_url() . 'assets' . '/images' . '/pengurus' . '/' . $_FILES['nama_gambar']['name'];
            $rm_space = preg_replace('/\s+/', '_', $new_names);
            $config['upload_path']   = './assets/images/pengurus';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $new_name;


            $this->upload->initialize($config);

            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {

                    //  $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'nama'      => $nama,
                        'alamat' => $alamat,
                        'foto'      =>  $rm_space,
                        'deskripsi'      =>  $jabatan,
                        'afiliasi'      =>  $afiliasi,
                    );
                    $this->M_user_pengurus->insert_pengurus($data);
                    $this->session->set_flashdata('msg', show_succ_msg('Data Pengurus Berhasil ditambahkan', '20px'));
                    redirect('user_pengurus');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Data Pengurus Gagal ditambahkan', '20px'));
                    redirect('user_pengurus');
                }
            }
        }
    }

    public function pengurus_edit()
    {
        $id = $this->input->post('id');
        $nama        = $this->input->post('nama');
        $alamat    = $this->input->post('alamat');
        $jabatan    = $this->input->post('jabatan');
        $afiliasi    = $this->input->post('afiliasi');

        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim');
        $this->form_validation->set_rules('jabatan', 'jabatan', 'trim');
        $this->form_validation->set_rules('afiliasi', 'afiliasi', 'trim');

        if ($this->form_validation->run() !== FALSE) {
            $id = array('id' => $id);

            $new_name = $_FILES['nama_gambar']['name'];
            $new_names = base_url() . 'assets' . '/images' . '/pengurus' . '/' . $_FILES['nama_gambar']['name'];
            $rm_space = preg_replace('/\s+/', '_', $new_names);

            $config['upload_path']   = './assets/images/pengurus';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $new_name;

            $this->upload->initialize($config);
            if (!empty($_FILES['nama_gambar']['name'])) {
                if ($this->upload->do_upload('nama_gambar')) {
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'nama'      => $nama,
                        'alamat' => $alamat,
                        'foto'      =>  $rm_space,
                        'deskripsi'      =>  $jabatan,
                        'afiliasi'      =>  $afiliasi,
                    );
                    $this->M_user_pengurus->update_pengurus($data, $id);
                    $this->session->set_flashdata('msg', show_succ_msg('Data Pengurus Berhasil diupdate', '20px'));
                    redirect('user_pengurus');
                } else {
                    $this->session->set_flashdata('msg', show_err_msg('Data Pengurus Gagal diupdate', '20px'));
                    redirect('user_pengurus');
                }
            } else {
                $data = array(
                    'nama'      => $nama,
                    'alamat' => $alamat,
                    'deskripsi'      =>  $jabatan,
                    'afiliasi'      =>  $afiliasi,
                );
                $this->M_user_pengurus->update_pengurus($data, $id);
                $this->session->set_flashdata('msg', show_succ_msg('Data Pengurus Berhasil diupdate', '20px'));
                redirect('user_pengurus');
            }
        }
    }

    public function pengurus_delete($id)
    {
        $where = array('id' => $id);
        $this->M_user_pengurus->delete_pengurus($where, 'u_pengurus');
        $this->session->set_flashdata('msg', show_succ_msg('Data Pengurus Berhasil dihapus', '20px'));
        redirect('user_pengurus');
    }

    function description()
    {
        $data['userdata']     = $this->userdata;

        $data['page']         = "desc_pengurus";
        $data['judul']         = "Deskripsi Pengurus";
        $data['deskripsi']     = "Deskripsi Pengurus";

        $data['desc_pengurus'] = $this->M_user_pengurus->get_desc_pengurus_all();
        $this->template->views('admin_view/user_pengurus/sec_title', $data);
    }

    function description_add()
    {
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                'judul'      => $judul,
                'deskripsi' => $deskripsi,
            );
            $this->M_user_pengurus->insert_deskripsi($data);
            $this->session->set_flashdata('msg', show_succ_msg('Deskripsi Berhasil ditambahkan', '20px'));
            redirect('user_pengurus/description');
        } else {
            $this->session->set_flashdata('msg', show_err_msg('Deskripsi Gagal ditambahkan', '20px'));
            redirect('user_pengurus/description');
        }
    }

    function description_edit()
    {
        $id = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $id = array('id' => $id);
            $data = array(
                'judul'      => $judul,
                'deskripsi' => $deskripsi,
            );
            $this->M_user_pengurus->update_deskripsi($data, $id);
            $this->session->set_flashdata('msg', show_succ_msg('Deskripsi Berhasil diupdate', '20px'));
            redirect('user_pengurus/description');
        } else {
            $this->session->set_flashdata('msg', show_err_msg('Deskripsi Gagal diupdate', '20px'));
            redirect('user_pengurus/description');
        }
    }

    public function description_delete($id)
    {
        $where = array('id' => $id);
        $this->M_user_pengurus->delete_deskripsi($where, 'pengurus_desc');
        $this->session->set_flashdata('msg', show_succ_msg('Deskripsi Berhasil dihapus', '20px'));
        redirect('user_pengurus/description');
    }
}
