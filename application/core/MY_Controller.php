<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

    protected $data = array(); //protected variables goes here its declaration

    function __construct()
    {

        parent::__construct();
        $this->output->enable_profiler(FALSE); // I keep this here so I dont have to manualy edit each controller to see profiler or not        
        $this->load->model('M_berita'); //this can be also done in autoload...
        $this->load->model('M_jurnal'); //this can be also done in autoload...
        //load helpers and everything here like form_helper etc
    }

    public function get_berita_pag()
    {
        $perpage = 6;
        $config['base_url'] = site_url('/page/view/berita');
        $config['total_rows'] = $this->M_berita->get_all_berita()->num_rows();
        $config['per_page'] = $perpage;
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['full_tag_open'] = '<ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li class="page-link">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a>';
        $config['prev_tag_open'] = '<li class="page-link">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="page-link">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-link">';
        $config['last_tag_open'] = '<li class="page-link">';
        $config['first_tag_open'] = '<li class="page-link">';
        $config['first_tag_open'] = '<li class="page-link">';
        $this->pagination->initialize($config);
    }

    public function get_jurnal_pag()
    {
        $perpage = 6;
        $config['base_url'] = site_url('/page/view/jurnal');
        $config['total_rows'] = $this->M_jurnal->get_all_jurnal()->num_rows();
        $config['per_page'] = $perpage;
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['full_tag_open'] = '<ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li class="page-link">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a>';
        $config['prev_tag_open'] = '<li class="page-link">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="page-link">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-link">';
        $config['last_tag_open'] = '<li class="page-link">';
        $config['first_tag_open'] = '<li class="page-link">';
        $config['first_tag_open'] = '<li class="page-link">';
        $this->pagination->initialize($config);
    }
}
