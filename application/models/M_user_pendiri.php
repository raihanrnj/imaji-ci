<?php
class M_user_pendiri extends CI_Model
{

    function get_pendiri_all()
    {
        $query = $this->db->get('u_pendiri');
        return $query->result_array();
    }

    public function insert_pendiri($data)
    {
        $this->db->insert('u_pendiri', $data);
        return TRUE;
    }

    public function update_pendiri($data, $id)
    {
        $this->db->update('u_pendiri', $data, $id);
        return TRUE;
    }

    function delete_pendiri($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function get_provinsi()
    {
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get('provinces');
        return $query->result();
    }

    function get_desc_pendiri_all()
    {
        $query = $this->db->get('pendiri_desc');
        return $query->result_array();
    }

    public function insert_deskripsi($data)
    {
        $this->db->insert('pendiri_desc', $data);
        return TRUE;
    }

    public function update_deskripsi($data, $id)
    {
        $this->db->update('pendiri_desc', $data, $id);
        return TRUE;
    }

    function delete_deskripsi($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
