<?php

class M_user_keanggotaan extends CI_Model
{

    function get_keanggotaan()
    {
        $query = $this->db->get('u_keanggotaan_syarat');
        return $query->result_array();
    }

    public function insert($data)
    {
        $this->db->insert('u_keanggotaan_syarat', $data);
        return TRUE;
    }

    public function update($data, $data_id)
    {
        $this->db->update('u_keanggotaan_syarat', $data, $data_id);
        return TRUE;
    }


    function delete_keanggotaan($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }


    // Info Keanggotaan
    function get_info_keanggotaan()
    {
        $query = $this->db->get('u_keanggotaan_pendaftaran');
        return $query->result_array();
    }

    public function insert_info_keanggotaan($data)
    {
        $this->db->insert('u_keanggotaan_pendaftaran', $data);
        return TRUE;
    }
    public function update_info_keanggotaan($data, $data_id)
    {
        $this->db->update('u_keanggotaan_pendaftaran', $data, $data_id);
        return TRUE;
    }
    function delete_info_keanggotaan($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
