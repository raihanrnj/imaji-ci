<?php
class M_berita extends CI_Model
{

    function get_berita_by_kode($kode)
    {
        $hsl = $this->db->query("SELECT * FROM berita WHERE berita_id='$kode'");
        return $hsl;
    }

    function get_berita_terbaru()
    {
        $query = $this->db->query("SELECT * FROM berita ORDER BY berita_id DESC limit 3");
        // return $hsl;
        return $query->result_array();
    }

    function get_berita()
    {
        $query = $this->db->query("SELECT * FROM berita ORDER BY berita_id DESC limit 3");
        return $query->result_array();
    }

    function get_berita_all()
    {
        $query = $this->db->get('berita');
        return $query->result_array();
    }

    public function insert_berita($data)
    {
        $this->db->insert('berita', $data);
        return TRUE;
    }

    public function update_berita($data, $data_id)
    {
        $this->db->update('berita', $data, $data_id);
        return TRUE;
    }
    function delete_berita($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function get_all_berita()
    {
        $this->db->select('*');
        $this->db->from('berita');
        $this->db->order_by('berita_id', 'DESC');
        return $this->db->get();
    }

    public function getDataPagination($limit, $offset)
    {
        $this->db->select('*');
        $this->db->from('berita');
        $this->db->order_by('berita_id', 'DESC');
        $this->db->limit($limit, $offset);

        return $this->db->get();
    }

    public function getDataPagination_old($limit, $offset)
    {
        $this->db->select('*');
        $this->db->from('berita');
        $this->db->order_by('berita_id', 'ASC');
        $this->db->limit($limit, $offset);

        return $this->db->get();
    }
}
