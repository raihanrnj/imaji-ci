<?php

class M_user_welcome extends CI_Model
{

    public $table = 'u_welcome_slider';
    public $id = 'id';


    public function insert($data)
    {
        $this->db->insert('u_welcome_slider', $data);
        return TRUE;
    }

    public function update($data, $data_id)
    {
        $this->db->update('u_welcome_slider', $data, $data_id);
        return TRUE;
    }

    function get_slider()
    {

        $sql = "SELECT * FROM u_welcome_slider order by urutan ASC";
		$query = $this->db->query($sql);
        return $query->result_array();

    }

    function delete_slider($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }


    // intro
    function get_intro()
    {
        $query = $this->db->get('u_welcome_intro');
        return $query->result_array();
    }

    public function insert_intro($data)
    {
        $this->db->insert('u_welcome_intro', $data);
        return TRUE;
    }
    public function update_intro($data, $data_id)
    {
        $this->db->update('u_welcome_intro', $data, $data_id);
        return TRUE;
    }
    function delete_intro($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // gallery
    function get_galeri()
    {
        $query = $this->db->get('u_welcome_galeri');
        return $query->result_array();
    }

    public function insert_galeri($data)
    {
        $this->db->insert('u_welcome_galeri', $data);
        return TRUE;
    }
    public function update_galeri($data, $data_id)
    {
        $this->db->update('u_welcome_galeri', $data, $data_id);
        return TRUE;
    }
    function delete_galeri($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
