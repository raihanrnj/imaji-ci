<?php

class M_user_profiles extends CI_Model
{

    // public $table = 'u_profiles_pengantar';
    // public $id = 'id';


    function get_pengantar()
    {
        $query = $this->db->get('u_profiles_pengantar');
        return $query->result_array();
    }

    public function insert($data)
    {
        $this->db->insert('u_profiles_pengantar', $data);
        return TRUE;
    }

    public function update($data, $data_id)
    {
        $this->db->update('u_profiles_pengantar', $data, $data_id);
        return TRUE;
    }


    function delete_pengantar($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }


    // Akta Pendirian
    function get_akta_pendirian()
    {
        $query = $this->db->get('u_profiles_akta_pendirian');
        return $query->result_array();
    }

    public function insert_akta_pendirian($data)
    {
        $this->db->insert('u_profiles_akta_pendirian', $data);
        return TRUE;
    }
    public function update_akta_pendirian($data, $data_id)
    {
        $this->db->update('u_profiles_akta_pendirian', $data, $data_id);
        return TRUE;
    }
    function delete_akta_pendirian($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }



    // Akta lambang dan arti
    function get_lambang_dan_arti()
    {
        $query = $this->db->get('u_profiles_lambang_dan_arti');
        return $query->result_array();
    }
    public function insert_lambang_dan_arti($data)
    {
        $this->db->insert('u_profiles_lambang_dan_arti', $data);
        return TRUE;
    }
    public function update_lambang_dan_arti($data, $data_id)
    {
        $this->db->update('u_profiles_lambang_dan_arti', $data, $data_id);
        return TRUE;
    }
    function delete_lambang_dan_arti($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // visi misi
    function get_visi_misi()
    {
        $query = $this->db->get('u_profiles_visi_misi');
        return $query->result_array();
    }
    public function insert_visi_misi($data)
    {
        $this->db->insert('u_profiles_visi_misi', $data);
        return TRUE;
    }
    public function update_visi_misi($data, $data_id)
    {
        $this->db->update('u_profiles_visi_misi', $data, $data_id);
        return TRUE;
    }
    function delete_visi_misi($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // kode etik
    function get_kode_etik()
    {
        $query = $this->db->get('u_profiles_kode_etik');
        return $query->result_array();
    }
    public function insert_kode_etik($data)
    {
        $this->db->insert('u_profiles_kode_etik', $data);
        return TRUE;
    }
    public function update_kode_etik($data, $data_id)
    {
        $this->db->update('u_profiles_kode_etik', $data, $data_id);
        return TRUE;
    }
    function delete_kode_etik($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
