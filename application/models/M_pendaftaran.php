<?php

class M_pendaftaran extends CI_Model
{

    public $table = 'anggota';
    public $id = 'id';

    public function insert_pendaftaran($data)
    {
        $this->db->insert('anggota', $data);
        return TRUE;
    }

    public function get_maxKodeAnggota()
    {
            $sql = "SELECT MAX(id) as value FROM anggota";
            $query = $this->db->query($sql);
            $maxValue = strval($query->result_array()[0]['value']);
            $maxKode_anggota = date('Ymd').sprintf("%04d", $maxValue);
          return $maxKode_anggota;
            
    }
}
