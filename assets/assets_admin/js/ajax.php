<script type="text/javascript">
	var MyTable = $('#list-data').dataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false
	});

	window.onload = function() {
		tampilAnggota();
		tampilValidasianggota();
		tampilPegawai();
		tampilPosisi();
		tampilKota();
		<?php
		if ($this->session->flashdata('msg') != '') {
			echo "effect_msg();";
		}
		?>
	}

	function refresh() {
		MyTable = $('#list-data').dataTable();
	}

	function effect_msg_form() {
		// $('.form-msg').hide();
		$('.form-msg').show(1000);
		setTimeout(function() {
			$('.form-msg').fadeOut(1000);
		}, 3000);
	}

	function effect_msg() {
		// $('.msg').hide();
		$('.msg').show(1000);
		setTimeout(function() {
			$('.msg').fadeOut(1000);
		}, 3000);
	}

	function tampilPegawai() {
		$.get('<?php echo base_url('Pegawai/tampil'); ?>', function(data) {
			MyTable.fnDestroy();
			$('#data-pegawai').html(data);
			refresh();
		});
	}

	function tampilAnggota() {
		$.get('<?php echo base_url('Anggota/tampil'); ?>', function(data) {
			MyTable.fnDestroy();
			$('#data-anggota').html(data);
			refresh();
		});
	}
	function tampilValidasianggota() {
		$.get('<?php echo base_url('Validasi_anggota/tampil'); ?>', function(data) {
			MyTable.fnDestroy();
			$('#data-validasianggota').html(data);
			refresh();
		});
	}

	//Pegawai
	var id_pegawai;
	$(document).on("click", ".konfirmasiHapus-pegawai", function() {
		id_pegawai = $(this).attr("data-id");
	})
	$(document).on("click", ".hapus-dataPegawai", function() {
		var id = id_pegawai;

		$.ajax({
				method: "POST",
				url: "<?php echo base_url('Pegawai/delete'); ?>",
				data: "id=" + id
			})
			.done(function(data) {
				$('#konfirmasiHapus').modal('hide');
				tampilPegawai();
				$('.msg').html(data);
				effect_msg();
			})
	})

	$(document).on("click", ".update-dataPegawai", function() {
		var id = $(this).attr("data-id");

		$.ajax({
				method: "POST",
				url: "<?php echo base_url('Pegawai/update'); ?>",
				data: "id=" + id
			})
			.done(function(data) {
				$('#tempat-modal').html(data);
				$('#update-pegawai').modal('show');
			})
	})

	$('#form-tambah-pegawai').submit(function(e) {
		var data = $(this).serialize();
	

		$.ajax({
				method: 'POST',
				url: '<?php echo base_url('Pegawai/prosesTambah'); ?>',
				data: data
			})
			.done(function(data) {
				var out = jQuery.parseJSON(data);

				tampilPegawai();
				if (out.status == 'form') {
					$('.form-msg').html(out.msg);
					effect_msg_form();
				} else {
					document.getElementById("form-tambah-pegawai").reset();
					$('#tambah-pegawai').modal('hide');
					$('.msg').html(out.msg);
					effect_msg();
				}
			})

		e.preventDefault();
	});

	$(document).on('submit', '#form-update-pegawai', function(e) {
		var data = $(this).serialize();
		console.log("ini nih : ", data)

		$.ajax({
				method: 'POST',
				url: '<?php echo base_url('Pegawai/prosesUpdate'); ?>',
				data: data
			})
			.done(function(data) {
				var out = jQuery.parseJSON(data);

				tampilPegawai();
				if (out.status == 'form') {
					$('.form-msg').html(out.msg);
					effect_msg_form();
				} else {
					document.getElementById("form-update-pegawai").reset();
					$('#update-pegawai').modal('hide');
					$('.msg').html(out.msg);
					effect_msg();
				}
			})

		e.preventDefault();
	});

	$('#tambah-pegawai').on('hidden.bs.modal', function() {
		$('.form-msg').html('');
	})

	$('#update-pegawai').on('hidden.bs.modal', function() {
		$('.form-msg').html('');
	})

	//Anggota
	var id_anggota;
	$(document).on("click", ".konfirmasiHapus-anggota", function() {
		console.log("hayoo");
		id_anggota = $(this).attr("data-id");
	})
	$(document).on("click", ".hapus-dataAnggota", function() {
	
		var id = id_anggota;

		$.ajax({
				method: "POST",
				url: "<?php echo base_url('Anggota/delete'); ?>",
				data: "id=" + id
			})
			.done(function(data) {
				$('#konfirmasiHapus').modal('hide');
				tampilAnggota();
				$('.msg').html(data);
				effect_msg();
			})
	})

	$(document).on("click", ".update-dataAnggota", function() {
		var id = $(this).attr("data-id");

		$.ajax({
				method: "POST",
				url: "<?php echo base_url('Anggota/update'); ?>",
				data: "id=" + id
			})
			.done(function(data) {
				$('#tempat-modal').html(data);
				$('#update-anggota').modal('show');
			})
	})

	$('#form-tambah-anggota').submit(function(e) {
		var data = $(this).serialize();

		$.ajax({
				method: 'POST',
				url: '<?php echo base_url('Anggota/prosesTambah'); ?>',
				data: data
			})
			.done(function(data) {
				var out = jQuery.parseJSON(data);

				tampilAnggota();
				if (out.status == 'form') {
					$('.form-msg').html(out.msg);
					effect_msg_form();
				} else {
					document.getElementById("form-tambah-anggota").reset();
					$('#tambah-anggota').modal('hide');
					$('.msg').html(out.msg);
					effect_msg();
				}
			})

		e.preventDefault();
	});

	$(document).on('submit', '#form-update-anggota', function(e) {
		var data = $(this).serialize();

		$.ajax({
				method: 'POST',
				url: '<?php echo base_url('Anggota/prosesUpdate'); ?>',
				data: data
			})
			.done(function(data) {
				var out = jQuery.parseJSON(data);

				tampilAnggota();
				if (out.status == 'form') {
					$('.form-msg').html(out.msg);
					effect_msg_form();
				} else {
					document.getElementById("form-update-anggota").reset();
					$('#update-anggota').modal('hide');
					$('.msg').html(out.msg);
					effect_msg();
				}
			})

		e.preventDefault();
	});

	$('#tambah-anggota').on('hidden.bs.modal', function() {
		$('.form-msg').html('');
	})

	$('#update-anggota').on('hidden.bs.modal', function() {
		$('.form-msg').html('');
	})

	//Validasi Anggota
	var id_anggota;
	$(document).on("click", ".konfirmasiHapus-validasianggota", function() {
		console.log("hayoo");
		id_anggota = $(this).attr("data-id");
	})
	$(document).on("click", ".hapus-dataValidasianggota", function() {
	
		var id = id_anggota;

		$.ajax({
				method: "POST",
				url: "<?php echo base_url('Validasi_anggota/delete'); ?>",
				data: "id=" + id
			})
			.done(function(data) {
				$('#konfirmasiHapus').modal('hide');
				tampilValidasianggota();
				$('.msg').html(data);
				effect_msg();
			})
	})

	$(document).on("click", ".update-dataValidasianggota", function() {
		var id = $(this).attr("data-id");
		console.log(id);

		$.ajax({
				method: "POST",
				url: "<?php echo base_url('Validasi_anggota/update'); ?>",
				data: "id=" + id
			})
			.done(function(data) {
				$('#tempat-modal').html(data);
				$('#update-validasianggota').modal('show');
			})

		//LOAD PROVINCE AND KABUPATEN
		// var provinsi_id = $("#provinsi").val(); //ambil value id dari provinsi
		// if (provinsi_id != "") {
		// 	$.ajax({
		// 		url: base_url + "/imaji/pendaftaran/get_kabupaten",
		// 		method: "POST",
		// 		data: {
		// 			provinsi_id: provinsi_id,
		// 		},
		// 		success: function(data) {
		// 			$("#kota").html(data);
		// 		},
		// 	});
		// }
	})

	$(document).on('submit', '#form-update-validasianggota', function(e) {
		var data = $(this).serialize();
		console.log("ini data : ",data);

		$.ajax({
				method: 'POST',
				url: '<?php echo base_url('Validasi_anggota/prosesUpdate'); ?>',
				data: data
			})
			.done(function(data) {
				var out = jQuery.parseJSON(data);

				tampilValidasianggota();
				// console.log(out);
				if (out.status == 'form') {
					$('.form-msg').html(out.msg);
					effect_msg_form();
				} else {
					document.getElementById("form-update-validasianggota").reset();
					$('#update-validasianggota').modal('hide');
					$('.msg').html(out.msg);
					effect_msg();
				}
			})

		e.preventDefault();
	});


	$('#update-validasianggota').on('hidden.bs.modal', function() {
		$('.form-msg').html('');
	})


	//Kota
	function tampilKota() {
		$.get('<?php echo base_url('Kota/tampil'); ?>', function(data) {
			MyTable.fnDestroy();
			$('#data-kota').html(data);
			refresh();
		});
	}

	var id_kota;
	$(document).on("click", ".konfirmasiHapus-kota", function() {
		id_kota = $(this).attr("data-id");
	})
	$(document).on("click", ".hapus-dataKota", function() {
		var id = id_kota;

		$.ajax({
				method: "POST",
				url: "<?php echo base_url('Kota/delete'); ?>",
				data: "id=" + id
			})
			.done(function(data) {
				$('#konfirmasiHapus').modal('hide');
				tampilKota();
				$('.msg').html(data);
				effect_msg();
			})
	})

	$(document).on("click", ".update-dataKota", function() {
		var id = $(this).attr("data-id");

		$.ajax({
				method: "POST",
				url: "<?php echo base_url('Kota/update'); ?>",
				data: "id=" + id
			})
			.done(function(data) {
				$('#tempat-modal').html(data);
				$('#update-kota').modal('show');
			})
	})

	$(document).on("click", ".detail-dataKota", function() {
		var id = $(this).attr("data-id");

		$.ajax({
				method: "POST",
				url: "<?php echo base_url('Kota/detail'); ?>",
				data: "id=" + id
			})
			.done(function(data) {
				$('#tempat-modal').html(data);
				$('#tabel-detail').dataTable({
					"paging": true,
					"lengthChange": false,
					"searching": true,
					"ordering": true,
					"info": true,
					"autoWidth": false
				});
				$('#detail-kota').modal('show');
			})
	})

	$('#form-tambah-kota').submit(function(e) {
		var data = $(this).serialize();

		$.ajax({
				method: 'POST',
				url: '<?php echo base_url('Kota/prosesTambah'); ?>',
				data: data
			})
			.done(function(data) {
				var out = jQuery.parseJSON(data);

				tampilKota();
				if (out.status == 'form') {
					$('.form-msg').html(out.msg);
					effect_msg_form();
				} else {
					document.getElementById("form-tambah-kota").reset();
					$('#tambah-kota').modal('hide');
					$('.msg').html(out.msg);
					effect_msg();
				}
			})

		e.preventDefault();
	});

	$(document).on('submit', '#form-update-kota', function(e) {
		var data = $(this).serialize();

		$.ajax({
				method: 'POST',
				url: '<?php echo base_url('Kota/prosesUpdate'); ?>',
				data: data
			})
			.done(function(data) {
				var out = jQuery.parseJSON(data);

				tampilKota();
				if (out.status == 'form') {
					$('.form-msg').html(out.msg);
					effect_msg_form();
				} else {
					document.getElementById("form-update-kota").reset();
					$('#update-kota').modal('hide');
					$('.msg').html(out.msg);
					effect_msg();
				}
			})

		e.preventDefault();
	});

	$('#tambah-kota').on('hidden.bs.modal', function() {
		$('.form-msg').html('');
	})

	$('#update-kota').on('hidden.bs.modal', function() {
		$('.form-msg').html('');
	})

	//Posisi
	function tampilPosisi() {
		$.get('<?php echo base_url('Posisi/tampil'); ?>', function(data) {
			MyTable.fnDestroy();
			$('#data-posisi').html(data);
			refresh();
		});
	}

	var id_posisi;
	$(document).on("click", ".konfirmasiHapus-posisi", function() {
		id_posisi = $(this).attr("data-id");
	})
	$(document).on("click", ".hapus-dataPosisi", function() {
		var id = id_posisi;

		$.ajax({
				method: "POST",
				url: "<?php echo base_url('Posisi/delete'); ?>",
				data: "id=" + id
			})
			.done(function(data) {
				$('#konfirmasiHapus').modal('hide');
				tampilPosisi();
				$('.msg').html(data);
				effect_msg();
			})
	})

	$(document).on("click", ".update-dataPosisi", function() {
		var id = $(this).attr("data-id");

		$.ajax({
				method: "POST",
				url: "<?php echo base_url('Posisi/update'); ?>",
				data: "id=" + id
			})
			.done(function(data) {
				$('#tempat-modal').html(data);
				$('#update-posisi').modal('show');
			})
	})

	$(document).on("click", ".detail-dataPosisi", function() {
		var id = $(this).attr("data-id");

		$.ajax({
				method: "POST",
				url: "<?php echo base_url('Posisi/detail'); ?>",
				data: "id=" + id
			})
			.done(function(data) {
				$('#tempat-modal').html(data);
				$('#tabel-detail').dataTable({
					"paging": true,
					"lengthChange": false,
					"searching": true,
					"ordering": true,
					"info": true,
					"autoWidth": false
				});
				$('#detail-posisi').modal('show');
			})
	})

	$('#form-tambah-posisi').submit(function(e) {
		var data = $(this).serialize();

		$.ajax({
				method: 'POST',
				url: '<?php echo base_url('Posisi/prosesTambah'); ?>',
				data: data
			})
			.done(function(data) {
				var out = jQuery.parseJSON(data);

				tampilPosisi();
				if (out.status == 'form') {
					$('.form-msg').html(out.msg);
					effect_msg_form();
				} else {
					document.getElementById("form-tambah-posisi").reset();
					$('#tambah-posisi').modal('hide');
					$('.msg').html(out.msg);
					effect_msg();
				}
			})

		e.preventDefault();
	});

	$(document).on('submit', '#form-update-posisi', function(e) {
		var data = $(this).serialize();
		console.log(data);

		$.ajax({
				method: 'POST',
				url: '<?php echo base_url('Posisi/prosesUpdate'); ?>',
				data: data
			})
			.done(function(data) {
				var out = jQuery.parseJSON(data);

				tampilPosisi();
				if (out.status == 'form') {
					$('.form-msg').html(out.msg);
					effect_msg_form();
				} else {
					document.getElementById("form-update-posisi").reset();
					$('#update-posisi').modal('hide');
					$('.msg').html(out.msg);
					effect_msg();
				}
			})

		e.preventDefault();
	});

	$('#tambah-posisi').on('hidden.bs.modal', function() {
		$('.form-msg').html('');
	})

	$('#update-posisi').on('hidden.bs.modal', function() {
		$('.form-msg').html('');
	})

	$('.textarea').wysihtml5()
</script>