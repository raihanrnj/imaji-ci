var base_url = window.location.origin;

$(function() {
    $('input[name="tanggal_lahir"]').daterangepicker({
        singleDatePicker: true,
        autoUpdateInput: false,
        autoApply: true,
        showDropdowns: true,
        maxDate: new Date(),
        locale: {
            cancelLabel: "Clear",
        },
    });

    $('input[name="tanggal_lahir"]').on(
        "apply.daterangepicker",
        function(ev, picker) {
            $(this).val(picker.startDate.format("Y-MM-DD"));
        }
    );

    $('input[name="tanggal_lahir"]').on(
        "cancel.daterangepicker",
        function(ev, picker) {
            $(this).val("");
        }
    );
});

$(document).ready(function() {
    $("#nama_jurnal").prop("hidden", "true");
    $("#pic").prop("hidden", "true");
    $("#issn").prop("hidden", "true");
    $("#url_jurnal").prop("hidden", "true");
    $("#nim").prop("hidden", "true");
    $("#foto_ktm").prop("hidden", "true");
    $("#administrasi_mahasiswa_value").prop("hidden", "true");

    $("#profesi").change(function() {
        console.log($("#profesi option:selected").val());

        if ($("#profesi option:selected").val() == "pengelola_jurnal") {
            $("#nama_jurnal").prop("hidden", false);
            $("#pic").prop("hidden", false);
            $("#issn").prop("hidden", false);
            $("#url_jurnal").prop("hidden", false);
            $("#administrasi_mahasiswa_value").removeAttr("name");
            $("#administrasi_mahasiswa_value").prop("disabled", "true");
            $("#nima").prop("required", false);
            $("#foto_ktma").prop("required", false);
            $("#nima").val('');
            $("#foto_ktma").val('');
        } else {
            $("#nama_jurnal").prop("hidden", "true");
            $("#pic").prop("hidden", "true");
            $("#issn").prop("hidden", "true");
            $("#url_jurnal").prop("hidden", "true");
            $("#nima").prop("required", "true");
            $("#foto_ktma").prop("required", "true");
            $("#administrasi_mahasiswa_value").attr("name", "administrasi_profesi");
            $("#administrasi_mahasiswa_value").prop("disabled", false);
        }

        if ($("#profesi option:selected").val() == "mahasiswa") {
            $("#nim").prop("hidden", false);
            $("#foto_ktm").prop("hidden", false);
            $("#administrasi_mahasiswa_value").attr("name");
            $("#administrasi_mahasiswa_value").prop("hidden", false);
            $("#administrasi_profesi_value").prop("disabled", "true");
            $("#administrasi_profesi_value").prop("hidden", "true");
            $("#nama_jurnala").prop("required", false);
            $("#pica").prop("required", false);
            $("#issna").prop("required", false);
            $("#url_jurnala").prop("required", false);
            $("#nama_jurnala").val('');
            $("#pica").val('');
            $("#issna").val('');
            $("#url_jurnala").val('');
            $("#nama_jurnala").val('');

        } else {
            $("#nim").prop("hidden", "true");
            $("#foto_ktm").prop("hidden", "true");
            $("#administrasi_mahasiswa_value").prop("hidden", "true");
            $("#administrasi_profesi_value").prop("disabled", false);
            $("#administrasi_profesi_value").prop("hidden", false);
            $("#nama_jurnala").prop("required", "true");
            $("#pica").prop("required", "true");
            $("#issna").prop("required", "true");
            $("#url_jurnala").prop("required", "true");
        }
        if ($("#profesi option:selected").val() == "umum") {
            $("#nama_jurnala").prop("required", false);
            $("#pica").prop("required", false);
            $("#issna").prop("required", false);
            $("#url_jurnala").prop("required", false);
            $("#nima").prop("required", false);
            $("#foto_ktma").prop("required", false);
        }

        if ($("#profesi option:selected").val() == "dosen") {
            $("#nama_jurnala").prop("required", false);
            $("#pica").prop("required", false);
            $("#issna").prop("required", false);
            $("#url_jurnala").prop("required", false);
            $("#nima").prop("required", false);
            $("#foto_ktma").prop("required", false);
        }
    });
});

$(document).ready(function() {
    //request data kabupaten
    $("#provinsi").change(function() {
        var provinsi_id = $("#provinsi").val(); //ambil value id dari provinsi

        if (provinsi_id != "") {
            $.ajax({
                url: base_url + "/pendaftaran/get_kabupaten",
                method: "POST",
                data: {
                    provinsi_id: provinsi_id,
                },
                success: function(data) {
                    $("#kota").html(data);
                },
            });
        }
    });

    //request data kabupaten
    $("#provinsi_institusi").change(function() {
        var provinsi_id = $("#provinsi_institusi").val(); //ambil value id dari provinsi

        if (provinsi_id != "") {
            $.ajax({
                url: base_url + "/pendaftaran/get_kabupaten",
                method: "POST",
                data: {
                    provinsi_id: provinsi_id,
                },
                success: function(data) {
                    $("#kota_institusi").html(data);
                },
            });
        }
    });
});