var base_url = window.location.origin;

$.ajax({
    type: "POST",
    url: base_url + "/page/get_data_chart",
    data: {},
    success: function(response) {
        data = JSON.parse(response);

        chart_usia_penerima_permodalan(data);
    },
});

function chart_usia_penerima_permodalan(data) {
    console.log(data)
    Highcharts.chart("container", {
        chart: {
            height: 1200,
            inverted: true,
        },

        title: {
            text: "",
        },

        credits: {
            enabled: false,
        },

        exporting: {
            enabled: false,
        },

        accessibility: {
            point: {
                descriptionFormatter: function(point) {
                    var nodeName = point.toNode.name,
                        nodeId = point.toNode.id,
                        nodeDesc = nodeName === nodeId ? nodeName : nodeName + ", " + nodeId,
                        parentDesc = point.fromNode.id;
                    return (
                        point.index + ". " + nodeDesc + ", reports to " + parentDesc + "."
                    );
                },
            },
        },

        series: [{
            type: "organization",
            name: "",
            keys: ["from", "to"],
            data: [
                ["A", "B"],
                ["A", "C"],
                ["A", "D"],
                ["A", "E"],
                ["A", "F"],
                ["A", "G"],
                ["A", "H"],
                ["E", "I"],
            ],
            nodes: data['diagram'],
            colorByPoint: false,
            color: "white",
            height: 0,
            dataLabels: {
                color: "black",
            },
            borderColor: "#00000020",
            nodeWidth: 200,
        }, ],
        tooltip: {
            outside: true,
        },
    });
}
