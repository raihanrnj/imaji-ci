// data chart pengurus start
let data_pengurus = {
    key_1: {
        key_2: "",
        key_3: {
            key_6: "",
            key_7: "",
        },
        key_4: {
            key_8: "",
            key_9: "",
        },
        key_5: "",
    },
};

let treeParamsPengurus = {
    key_1: { trad: "Chair 1" },
    key_2: { trad: "Chair 2" },
    key_3: { trad: "Chair 3" },
    key_4: { trad: "Chair 4" },
    key_5: { trad: "Chair 5" },
    key_6: { trad: "Chair 6" },
    key_7: { trad: "Chair 7" },
    key_8: { trad: "Chair 8" },
    key_9: { trad: "Chair 9" },
};
// End data chart pengurus start

// Generate diagram Pengurus
treeMaker(data_pengurus, {
    id: "chart_pengurus",
    card_click: function(element) {
        console.log(element);
    },
    treeParams: treeParamsPengurus,
    link_width: "4px",
    link_color: "#2199e8",
});